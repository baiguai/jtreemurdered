/*
           Jreepad - personal information manager.
           Copyright (C) 2004 Dan Stowell

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

The full license can be read online here:

           http://www.gnu.org/copyleft/gpl.html
*/

package jreepad.editor;

import jreepad.JreepadView;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.awt.Window;
import java.text.StringCharacterIterator;
import java.text.CharacterIterator;
import java.awt.Component;
import javax.swing.text.*;
import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.html.HTMLDocument;
import java.awt.Font;
import javax.swing.JComponent;
import javax.swing.JTextPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.util.*;

import jreepad.JreepadPrefs;
import jreepad.JreepadArticle;
import jreepad.ui.FontHelper;
/**
 * Abstract article view in the form of a JEditorPane.
 *
 * @author <a href="mailto:pewu@losthive.org">Przemek Więch</a>
 * @version $Id: EditorPaneView.java,v 1.2 2008/09/21 11:06:50 danstowell Exp $
 */
public abstract class EditorPaneView
    extends AbstractArticleView {
    int findPos = 0;
    String findTx = "";

    protected JTextPane editorPane;

    public EditorPaneView(String type, JreepadArticle article) {
        super(article);
        editorPane = new JTextPane();
        reloadArticle();
    }

    public void reloadArticle()
    {
        // Reset the search
        findPos = 0;
        findTx = "";
        String tmp = "";
        int tsize = 0;

        tmp = JreepadView.getPrefs().tabsize;

        try
        {
            tsize = Integer.parseInt(tmp);
        }
        catch (Exception ex)
        {
            tsize = 4;
        }

        tmp = "";
        while (tmp.length() < tsize)
        {
            tmp += " ";
        }
        final String tabString = tmp;

        editorPane.setFont(new Font("monospaced", Font.PLAIN, 13));
        SetTabs(editorPane, tsize);

        editorPane.setText(article.getContent());
        editorPane.setBackground(Color.BLACK);
        editorPane.setForeground(Color.WHITE);
        editorPane.setCaretColor(Color.WHITE);

        // TABS CONVERSION
        Document doc = editorPane.getDocument();

        // Replacing tabs with spaces
        ((AbstractDocument) doc).setDocumentFilter(new DocumentFilter()
        {
            public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
            throws BadLocationException
            {
                if (editorPane.getSelectedText() == null)
                {
                    super.insertString(fb, offset, text.replace("\t", tabString), attrs);
                }
            }
        });
    }

    public void updateFont(int direction) {
        StyledEditorKit kit = (StyledEditorKit) editorPane.getEditorKit();
        MutableAttributeSet set = kit.getInputAttributes();
        StyledDocument doc = (StyledDocument) editorPane.getDocument();
        Font currentFont = doc.getFont(set);
        int currentFontSize = currentFont.getSize();

        switch (direction)
        {
            case FontHelper.FONT_DIR_UP:
                currentFontSize++;
                break;
            case FontHelper.FONT_DIR_DOWN:
                currentFontSize--;
                break;
        }

        editorPane.setFont(new Font("monospaced", Font.PLAIN, currentFontSize));
    }

  public JComponent getComponent() {
        return editorPane;
    }

    public String getText()
    {
        return editorPane.getText();
    }

    public String getSelectedText()
    {
        return editorPane.getSelectedText();
    }

    public void searchText()
    {
        findPos = 0;
        JTextArea srchTxt = new JTextArea(10, 60);

        srchTxt.addHierarchyListener(new HierarchyListener() 
        {
            public void hierarchyChanged(HierarchyEvent e) 
            {
                final Component c = e.getComponent();
                if (c.isShowing() && (e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) 
                {
                    Window toplevel = SwingUtilities.getWindowAncestor(c);
                    toplevel.addWindowFocusListener(new WindowAdapter() 
                    {
                        public void windowGainedFocus(WindowEvent e) 
                        {
                            c.requestFocus();
                        }
                    });
                }
            }
        });

        JPanel pane = new JPanel();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
        pane.add(srchTxt);

        int result = JOptionPane.showConfirmDialog(null, pane, 
            "Find Text", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION)
        {
            findTx = srchTxt.getText();
            if (findTx == null)
            {
                findTx = "";
                findPos = 0;
                return;
            }

            findTx = findTx.toLowerCase();
            String doc = editorPane.getText().toLowerCase();
            int srchStart = doc.indexOf(findTx);
            int len = findTx.length();
            editorPane.setFocusable(true);

            editorPane.requestFocus();
            // editorPane.select(srchStart, len);
            editorPane.select(srchStart, srchStart + len);
            findPos = srchStart + len;
        }
    }

    public void findNext()
    {
        if (findTx.length() < 1) searchText();

        String doc = editorPane.getText().toLowerCase();

        int srchStart = doc.indexOf(findTx, findPos);
        int len = findTx.length();

        // Loop back to the start if nothing found
        if (srchStart < 0)
        {
            srchStart = 0;
            findPos = 0;
        }
        srchStart = doc.indexOf(findTx, findPos);

        editorPane.requestFocus();
        editorPane.select(srchStart, srchStart + len);
        findPos = srchStart + len;
    }

    public void replaceText()
    {
        final JTextArea oldTxt = new JTextArea(10, 60);
        oldTxt.setText(getSelectedText());
        JTextArea newTxt = new JTextArea(10, 60);
        JPanel pane = new JPanel();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
        pane.add(oldTxt);
        pane.add(new JLabel(" "));
        pane.add(newTxt);

        int result = JOptionPane.showConfirmDialog(null, pane, 
            "Replace Text", JOptionPane.OK_CANCEL_OPTION);

        if (result == JOptionPane.OK_OPTION)
        {
            String oldStr = oldTxt.getText();
            String newStr = newTxt.getText();
            if (newStr == null) newStr = "";
            String doc = editorPane.getText().toLowerCase();
            doc = doc.replace(oldStr, newStr + "");

            editorPane.setText(doc);
        }
    }

    public void toUpperCase()
    {
        editorPane.replaceSelection(getSelectedText().toUpperCase());
    }

    public void toLowerCase()
    {
        if (getSelectedText() == null) return;

        editorPane.replaceSelection(getSelectedText().toLowerCase());
    }

    public void toList()
    {
        String str = getSelectedText();
        String textStr[] = str.split("\\r\\n|\\n|\\r");
        String output = "";
        int curNum = 1;
        String tmp = textStr.length + "";
        String pad = "";

        for (String s : textStr)
        {
            pad = curNum + "";
            while (pad.length() < tmp.length())
            {
                pad = "0" + pad;
            }
            if (!output.equals("")) output += "\n";
            output += pad + ". " + s;
            curNum++;
        }

        editorPane.replaceSelection(output);
    }

    public void formatTables()
    {
        String doc = editorPane.getText();
        String [] lines = doc.replaceAll("\\r", "").split("\\n");
        String [] tmp = null;
        boolean inTable = false;
        List<String> output = new ArrayList<String>();
        List<List<String>> tble = new ArrayList<List<String>>();
        List<String> row = new ArrayList<String>();
        String newTxt = "";

        for (String l : lines)
        {
            if (l.equalsIgnoreCase("[table]"))
            {
                inTable = true;
            }
            if (l.equalsIgnoreCase("[/table]"))
            {
                inTable = false;
            }

            if (inTable)
            {
                row = new ArrayList<String>();
                tmp = l.split(",");
            }
            else
            {
                output.add(l);
            }
        }
    }

    public void wrapSelectionToCharWidth(int wrapWidth)
    {
        if (wrapWidth < 2)
            return;

        StringBuffer ret = new StringBuffer();
        StringCharacterIterator iter = new StringCharacterIterator(editorPane.getSelectedText());
        int charsOnThisLine = 0;
        for (char c = iter.first(); c != CharacterIterator.DONE; c = iter.next())
        {
            if (c == '\n')
                charsOnThisLine = 0;
            else if (++charsOnThisLine >= wrapWidth)
            {
                ret.append('\n');
                charsOnThisLine = 0;
            }
            ret.append(c);
        }
        editorPane.replaceSelection(ret.toString());
    }

    public void addDivider()
    {
        String title = "";
        String hr = "";
        int slen = 0;
        int hrlen = 80;

        title = JOptionPane.showInputDialog(null, "Enter a title (optional):");

        if (title.equals(""))
        {
            editorPane.replaceSelection("--------------------------------------------------------------------------------");
        }
        else
        {
            slen = title.length() + 6;
            hr = "--[ " + title.toUpperCase() + " ]";

            for (int i = 0; i < hrlen - slen; i++)
            {
                hr += "-";
            }

            editorPane.replaceSelection(hr);
        }
    }





    // Set up the default tab size.
    // TODO: Update this to be driven by an option setting.
    public void SetTabs(JTextPane textPane, int charactersPerTab)
    {
        textPane.setFont(new Font("monospaced", Font.PLAIN, 13));
        FontMetrics fm = textPane.getFontMetrics( textPane.getFont() );
        int charWidth = fm.charWidth( ' ' );

        // Adjust the char with to match spaces
        charWidth--;

        int tabWidth = charWidth * charactersPerTab;

        TabStop[] tabs = new TabStop[100];

        for (int j = 0; j < tabs.length; j++)
        {
            int tab = j + 1;
            tabs[j] = new TabStop( tab * tabWidth );
        }

        TabSet tabSet = new TabSet(tabs);
        SimpleAttributeSet attributes = new SimpleAttributeSet();
        StyleConstants.setTabSet(attributes, tabSet);
        StyleConstants.setForeground (attributes, new Color (250, 250, 250));
        int length = textPane.getDocument().getLength();
        textPane.getStyledDocument().setParagraphAttributes(0, length, attributes, true);
    }
}
