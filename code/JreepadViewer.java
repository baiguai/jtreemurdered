/*
           Jreepad - personal information manager.
           Copyright (C) 2004 Dan Stowell

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

The full license can be read online here:

           http://www.gnu.org/copyleft/gpl.html
*/


package jreepad;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ListIterator;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import jreepad.io.AutoDetectReader;
import jreepad.io.EncryptedWriter;
import jreepad.io.HtmlWriter;
import jreepad.io.JreepadReader;
import jreepad.io.JreepadWriter;
import jreepad.io.TreepadWriter;
import jreepad.io.XmlWriter;
import jreepad.ui.PasswordDialog;
import jreepad.ui.SaveFileChooser;
import edu.stanford.ejalbert.BrowserLauncher;
import edu.stanford.ejalbert.exception.BrowserLaunchingExecutionException;
import edu.stanford.ejalbert.exception.BrowserLaunchingInitializingException;
import edu.stanford.ejalbert.exception.UnsupportedOperatingSystemException;
import jreepad.ui.FontHelper;

public class JreepadViewer
    extends JFrame { // implements ApplicationListener
  // Jreepad version, to appear in "about" box etc
  public static String version = "1.6 rc1";

  private static Vector theApps = new Vector(1,1);
  private Box toolBar, toolBarIconic;
  private JreepadView theJreepad;
  private JreepadTreeModel document;
  private Container content;

  private JFileChooser fileChooser;

  private String windowTitle;

  protected Clipboard systemClipboard;

  private JButton addAboveButton;
  private JButton addBelowButton;
  private JButton addButton;
  private JButton removeButton;
  private JButton upButton;
  private JButton downButton;
  private JButton indentButton;
  private JButton outdentButton;
  private JComboBox viewSelector, viewSelectorIconic;

  private JButton newIconButton;
  private JButton openIconButton;
  private JButton addAboveIconButton;
  private JButton addBelowIconButton;
  private JButton addIconButton;
  private JButton removeIconButton;
  private JButton upIconButton;
  private JButton downIconButton;
  private JButton indentIconButton;
  private JButton outdentIconButton;

  private Thread autoSaveThread;

  private JDialog autoSaveDialog;
  private JCheckBox autoSaveCheckBox;
  private JSpinner autoSavePeriodSpinner;
//  private DSpinner autoSavePeriodSpinner;
  private JButton autoSaveOkButton;
  private JButton autoSaveCancelButton;

  private JDialog prefsDialog;
  private SearchDialog searchDialog;

  private JDialog nodeUrlDisplayDialog;
  private JTextField nodeUrlDisplayField;
  private JButton nodeUrlDisplayOkButton;


    private JMenu openRecentMenu;
  private JMenuItem undoMenuItem;
  private JMenuItem redoMenuItem;
  private JMenuItem webSearchMenuItem;
  private JMenuItem characterWrapArticleMenuItem;
    private JCheckBoxMenuItem viewToolbarIconsMenuItem;
    private JCheckBoxMenuItem viewToolbarTextMenuItem;
    private JCheckBoxMenuItem viewToolbarOffMenuItem;

  private ColouredStrip funkyGreenStrip;

  // private boolean htmlExportOkChecker = false; // Just used to check whether OK or Cancel has been pressed in a certain dialogue box

  // Ask AWT which menu modifier we should be using.
  final static int MENU_MASK = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

  /*
  Note - The application code registered with Apple is:
  JREE� (Hex) 4A524545

  In Apple's Java methods they require an integer, so
  presumably we use 0x4A524545
  */
  public static final int appleAppCode = 0x4A524545;

  public JreepadViewer()
  {
    this("");
  }
  public JreepadViewer(String fileNameToLoad)
  {
    this(fileNameToLoad, null);
  }
  public JreepadViewer(String fileNameToLoad, String prefFilename)
  {
// THIS DOESN'T SEEM TO HAVE ANY EFFECT, ON MAC OSX - I'd be interested to know if it does its job on other OSs
// Apparently it does work on Windows. So I'll leave it in place!
    ClassLoader loader = this.getClass().getClassLoader(); // Used for loading icon
    java.net.URL iconUrl = loader.getResource("images/jreepadlogo-01-iconsize.gif");
    if(iconUrl != null){
      setIconImage(new ImageIcon(iconUrl).getImage());
    }else{
      // System.out.println("Icon image failed to load: images/jreepadlogo-01-iconsize.gif");
    }

    Toolkit theToolkit = getToolkit();
    Dimension wndSize = theToolkit.getScreenSize();
    systemClipboard = theToolkit.getSystemClipboard();

    // New method of loading prefs - using java's own API rather handling a file
    setPrefs(new JreepadPrefs(wndSize));
    if(!getPrefs().seenLicense) {
      showLicense();
      getPrefs().seenLicense = true;
    }
/*
    // Check if a preferences file exists - and if so, load it
    if(prefFilename!=null)
      prefsFile = new File(prefFilename);
    try
    {
      if(prefsFile.exists() && prefsFile.isFile())
      {
        ObjectInputStream prefsLoader = new ObjectInputStream(new FileInputStream(prefsFile));
        setPrefs((JreepadPrefs)prefsLoader.readObject());
        prefsLoader.close();
      }
      else
      {
        showLicense(); // A crude way of showing the license on first visit
        setPrefs(new JreepadPrefs(wndSize));
      }
    }
    catch(Exception err)
    {
      setPrefs(new JreepadPrefs(wndSize));
    }
*/

    fileChooser = new JFileChooser();
    fileChooser.addChoosableFileFilter(SaveFileChooser.JREEPAD_FILE_FILTER);
    fileChooser.addChoosableFileFilter(SaveFileChooser.JREEPAD_ENCRYPTED_FILE_FILTER);
    fileChooser.addChoosableFileFilter(SaveFileChooser.TREEPAD_FILE_FILTER);

    content = getContentPane();

    document = new JreepadTreeModel();
    theJreepad = new JreepadView(document);

    establishMenus();
    establishToolbar();
    searchDialog = new SearchDialog(this, theJreepad);
    prefsDialog = new PrefsDialog(this);
    establishAutosaveDialogue();
    establishNodeUrlDisplayDialogue();

    // Establish the autosave thread
    autoSaveThread = new Thread("Autosave thread")
                        {
                          public void run()
                          {
                            while(getPrefs().autoSave)
                            {
                              try
                              {
                                // Sleep for a bit...
                                sleep(60000L * getPrefs().autoSavePeriod);
                                yield();
                                // ...then if the saveLocation != null, trigger saveAction()
                                if(getPrefs().autoSave && document.getSaveLocation() != null)
                                  new SaveAction().actionPerformed(null);
                                else
                                  updateWindowTitle();
                              }
                              catch(InterruptedException e){}
                            }
                          }
                        };
    autoSaveThread.setPriority(Thread.MIN_PRIORITY);
    if(getPrefs().autoSave)
      autoSaveThread.start();
    // Finished establishing the autosave thread

    content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
    content.add(funkyGreenStrip = new ColouredStrip(new Color(0.09f, 0.4f, 0.12f), wndSize.width, 10) );
    funkyGreenStrip.setVisible(getPrefs().showGreenStrip);
    content.add(toolBar);
    content.add(toolBarIconic);
    setToolbarMode(getPrefs().toolbarMode);
    content.add(theJreepad);

    // Load the file - if it has been specified, and if it can be found, and if it's a valid HJT file
    File firstTimeFile = null;
    if(fileNameToLoad != "")
      firstTimeFile = new File(fileNameToLoad);
    else if(getPrefs().loadLastFileOnOpen && getPrefs().getMostRecentFile() != null)
      firstTimeFile = getPrefs().getMostRecentFile();

    if(firstTimeFile != null && firstTimeFile.isFile())
      openFile(firstTimeFile);

    // Set close operation
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent e){ quitAction(); }});

    setTitleBasedOnFilename("");

    // Finally, make the window visible and well-sized
    setBounds(getPrefs().windowLeft,getPrefs().windowTop,
              getPrefs().windowWidth, getPrefs().windowHeight);
    searchDialog.setBounds(getPrefs().windowWidth/2,getPrefs().windowHeight/6,
              (int)(getPrefs().windowWidth*0.7f),(int)(getPrefs().windowHeight*0.9f));
    autoSaveDialog.setBounds((int)(wndSize.width*0.5f),getPrefs().windowHeight/2,
              getPrefs().windowWidth/2, getPrefs().windowHeight/4);
    prefsDialog.setBounds(getPrefs().windowWidth/2,getPrefs().windowHeight/3,
              getPrefs().windowWidth, getPrefs().windowHeight);
    nodeUrlDisplayDialog.setBounds((int)(wndSize.width*0.1f),(int)(getPrefs().windowHeight*0.7f),
              (int)(getPrefs().windowWidth*1.3f), getPrefs().windowHeight/3);

    // pack() actually deprecates some of the functionality of the setBounds() calls just above
    //  - but hopefully gives a better mixture of sizes set programmatically and by the OS
    searchDialog.pack();
    autoSaveDialog.pack();
    prefsDialog.pack();
    nodeUrlDisplayDialog.pack();

    theApps.add(this);

    setVisible(true);
    // If loading the last-saved file, expand the nodes we last had open
    if(document.getSaveLocation() != null)
        theJreepad.expandPaths(getPrefs().treePathCollection.paths);

    theJreepad.returnFocusToTree();
  }

  // Used by the constructor
  private void establishMenus()
  {
    // Create the menu bar
    JMenuBar menuBar = new JMenuBar();

    // Add menus
    menuBar.add(createFileMenu());
    menuBar.add(createEditMenu());
    menuBar.add(createActionsMenu());
    menuBar.add(createViewMenu());
    menuBar.add(createOptionsMenu());
    menuBar.add(createHelpMenu());
    setJMenuBar(menuBar);
  }

  /**
   * Creates the File menu.
   */
  private JMenu createFileMenu()
  {
      JMenu fileMenu = new JMenu("File"); //"File");
      fileMenu.setMnemonic(KeyEvent.VK_F);

      JMenuItem newMenuItem = new JMenuItem("New"); //"New");
      newMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { newAction();}});
      fileMenu.add(newMenuItem);

      JMenuItem openMenuItem = new JMenuItem("Open"); //"Open");
      openMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {openAction();}});
      fileMenu.add(openMenuItem);

      openRecentMenu = new JMenu("Open Recent"); //"Open recent");
      updateOpenRecentMenu();
      fileMenu.add(openRecentMenu);

      fileMenu.add(new SaveAction()); // Save
      fileMenu.add(new SaveAction(true)); // Save As

      JMenuItem backupToMenuItem = new JMenuItem("Backup to"); //"Backup to...");
      backupToMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {backupToAction();}});
      fileMenu.add(backupToMenuItem);

      fileMenu.add(new JSeparator());

      JMenuItem printSubtreeMenuItem = new JMenuItem("Print Subtree"); //"Print subtree");
      printSubtreeMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {subtreeToBrowserForPrintAction();}});
      fileMenu.add(printSubtreeMenuItem);

      JMenuItem printArticleMenuItem = new JMenuItem("Print Article"); //"Print article");
      printArticleMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {articleToBrowserForPrintAction();}});
      fileMenu.add(printArticleMenuItem);

      fileMenu.add(new JSeparator());
      JMenu importMenu = new JMenu("Import"); //"Import...");
      fileMenu.add(importMenu);

      JMenuItem importHjtMenuItem = new JMenuItem("Import HJT"); //"...Treepad file as subtree");
      importHjtMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {importAction(FILE_FORMAT_HJT);}});
      importMenu.add(importHjtMenuItem);

      JMenuItem importTextMenuItem = new JMenuItem("Import TextFiles"); //"...text file(s) as child node(s)");
      importTextMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {importAction(FILE_FORMAT_TEXT);}});
      importMenu.add(importTextMenuItem);

      JMenuItem importTextAsListMenuItem = new JMenuItem("Import TextList"); //"...text list file, one-child-per-line");
      importTextAsListMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {importAction(FILE_FORMAT_TEXTASLIST);}});
      importMenu.add(importTextAsListMenuItem);

      fileMenu.add(createExportMenu());

      fileMenu.add(new JSeparator());
      JMenuItem quitMenuItem = new JMenuItem("Quit"); //"Quit");
      quitMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { quitAction(); }});
      fileMenu.add(quitMenuItem);
      quitMenuItem.setMnemonic('Q');
      quitMenuItem.setAccelerator(KeyStroke.getKeyStroke('Q', MENU_MASK));

      newMenuItem.setMnemonic('N');
      newMenuItem.setAccelerator(KeyStroke.getKeyStroke('N', MENU_MASK));
      openMenuItem.setMnemonic('O');
      openMenuItem.setAccelerator(KeyStroke.getKeyStroke('O', MENU_MASK));
      openRecentMenu.setMnemonic('R');
      printSubtreeMenuItem.setMnemonic('P');
      printSubtreeMenuItem.setAccelerator(KeyStroke.getKeyStroke('P', MENU_MASK));
      printArticleMenuItem.setAccelerator(KeyStroke.getKeyStroke('P', MENU_MASK | java.awt.Event.SHIFT_MASK));
      backupToMenuItem.setMnemonic('B');
      importMenu.setMnemonic('I');
      importHjtMenuItem.setMnemonic('f');
      importTextMenuItem.setMnemonic('t');
      importTextAsListMenuItem.setMnemonic('l');

      return fileMenu;
  }

  /**
   * Creates the Export submenu.
   */
  private JMenu createExportMenu()
  {
      JMenu exportMenu = new JMenu("Export"); //"Export selected...");

      JMenuItem exportHjtMenuItem = new JMenuItem("Export HJT"); //"...subtree to Treepad HJT file");
      exportHjtMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {exportAction(FILE_FORMAT_HJT);}});
      exportMenu.add(exportHjtMenuItem);

      JMenuItem exportHtmlMenuItem = new JMenuItem("Export HTML"); //"...subtree to HTML");
      exportHtmlMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {exportAction(FILE_FORMAT_HTML);}});
      exportMenu.add(exportHtmlMenuItem);

      JMenuItem exportSimpleXmlMenuItem = new JMenuItem("Export XML"); //"...subtree to XML");
      exportSimpleXmlMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {exportAction(FILE_FORMAT_XML);}});
      exportMenu.add(exportSimpleXmlMenuItem);

      JMenuItem exportListMenuItem = new JMenuItem("Export TextList"); //"...subtree to text list (node titles only)");
      exportListMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {exportAction(FILE_FORMAT_TEXTASLIST);}});
      exportMenu.add(exportListMenuItem);

      exportMenu.add(new JSeparator());

      JMenuItem exportTextMenuItem = new JMenuItem("Export Article"); //"...article to text file");
      exportTextMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {exportAction(FILE_FORMAT_TEXT);}});
      exportMenu.add(exportTextMenuItem);

      JMenuItem exportSubtreeTextMenuItem = new JMenuItem("Export Articles"); //"...subtree articles to text file");
      exportSubtreeTextMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {exportAction(FILE_FORMAT_ARTICLESTOTEXT);}});
      exportMenu.add(exportSubtreeTextMenuItem);

      exportMenu.setMnemonic('E');
      exportHjtMenuItem.setMnemonic('f');
      exportHtmlMenuItem.setMnemonic('h');
      exportSimpleXmlMenuItem.setMnemonic('x');
      exportTextMenuItem.setMnemonic('t');

      return exportMenu;
  }

  /**
   * Creates the Edit menu.
   */
  private JMenu createEditMenu()
  {
      JMenu editMenu = new JMenu("Edit"); //"Edit");
      editMenu.setMnemonic(KeyEvent.VK_E);

      undoMenuItem = new JMenuItem("Undo"); //"Undo");
      undoMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
          undoAction();
          updateUndoRedoMenuState();}});
      editMenu.add(undoMenuItem);

      redoMenuItem = new JMenuItem("Redo"); //"Redo");
      redoMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
          redoAction();
          updateUndoRedoMenuState();}});
      editMenu.add(redoMenuItem);

      editMenu.add(new JSeparator());

      JMenuItem addAboveMenuItem = new JMenuItem("Add Above"); //"Add sibling above");
      addAboveMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.addNodeAbove(); updateWindowTitle();}});
      editMenu.add(addAboveMenuItem);

      JMenuItem addBelowMenuItem = new JMenuItem("Add Below"); //"Add sibling below");
      addBelowMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.addNodeBelow(); updateWindowTitle();}});
      editMenu.add(addBelowMenuItem);

      JMenuItem addChildMenuItem = new JMenuItem("Add Child"); //"Add child");
      addChildMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.addNode(); updateWindowTitle(); }});
      editMenu.add(addChildMenuItem);

      editMenu.add(new JSeparator());

      JMenuItem newFromClipboardMenuItem = new JMenuItem("New From Clipboard"); //"New node from clipboard");
      newFromClipboardMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { systemClipboardToNewNode(); }});
      editMenu.add(newFromClipboardMenuItem);

      editMenu.add(new JSeparator());

      JMenuItem editNodeTitleMenuItem = new JMenuItem("Edit Node Title"); //"Edit node title");
      editNodeTitleMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.editNodeTitleAction(); }});
      editMenu.add(editNodeTitleMenuItem);

      editMenu.add(new JSeparator());

      JMenuItem deleteMenuItem = new JMenuItem("Delete Node"); //"Delete node");
      deleteMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { deleteNodeAction(); }});
      editMenu.add(deleteMenuItem);

      editMenu.add(new JSeparator());

      JMenuItem upMenuItem = new JMenuItem("Move Up"); //"Move node up");
      upMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.moveCurrentNodeUp(); theJreepad.returnFocusToTree(); updateWindowTitle(); }});
      editMenu.add(upMenuItem);

      JMenuItem downMenuItem = new JMenuItem("Move Down"); //"Move node down");
      downMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.moveCurrentNodeDown(); theJreepad.returnFocusToTree(); updateWindowTitle(); }});
      editMenu.add(downMenuItem);

      editMenu.add(new JSeparator());

      JMenuItem indentMenuItem = new JMenuItem("Move In"); //"Indent node (demote)");
      indentMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.indentCurrentNode(); theJreepad.returnFocusToTree(); updateWindowTitle(); }});
      editMenu.add(indentMenuItem);

      JMenuItem outdentMenuItem = new JMenuItem("Move Out"); //"Outdent node (promote)");
      outdentMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.outdentCurrentNode(); theJreepad.returnFocusToTree(); updateWindowTitle(); }});
      editMenu.add(outdentMenuItem);

      editMenu.add(new JSeparator());

      JMenuItem expandAllMenuItem = new JMenuItem("Expand"); //"Expand subtree");
      expandAllMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.expandAllCurrentNode(); }});
      editMenu.add(expandAllMenuItem);

      JMenuItem collapseAllMenuItem = new JMenuItem("Collapse"); //"Collapse subtree");
      collapseAllMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.collapseAllCurrentNode(); }});
      editMenu.add(collapseAllMenuItem);

      editMenu.add(new JSeparator());

      JMenuItem sortMenuItem = new JMenuItem("Sort One Level"); //"Sort children (one level)");
      sortMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.sortChildren(); updateWindowTitle(); }});
      editMenu.add(sortMenuItem);

      JMenuItem sortRecursiveMenuItem = new JMenuItem("Sort All Levels"); //"Sort children (all levels)");
      sortRecursiveMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.sortChildrenRecursive(); updateWindowTitle(); }});
      editMenu.add(sortRecursiveMenuItem);

      undoMenuItem.setMnemonic('u');
      undoMenuItem.setAccelerator(KeyStroke.getKeyStroke('Z', MENU_MASK));
      redoMenuItem.setMnemonic('r');
      redoMenuItem.setAccelerator(KeyStroke.getKeyStroke('Z', MENU_MASK | java.awt.event.InputEvent.SHIFT_MASK));
      addAboveMenuItem.setMnemonic('a');
      addAboveMenuItem.setAccelerator(KeyStroke.getKeyStroke('T', MENU_MASK));
      addBelowMenuItem.setMnemonic('b');
      addBelowMenuItem.setAccelerator(KeyStroke.getKeyStroke('B', MENU_MASK));
      addChildMenuItem.setMnemonic('c');
      addChildMenuItem.setAccelerator(KeyStroke.getKeyStroke('D', MENU_MASK));
      newFromClipboardMenuItem.setAccelerator(KeyStroke.getKeyStroke('M', MENU_MASK));
      editNodeTitleMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
      upMenuItem.setMnemonic('u');
      upMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_UP, MENU_MASK | java.awt.Event.ALT_MASK));
      downMenuItem.setMnemonic('d');
      downMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, MENU_MASK | java.awt.Event.ALT_MASK));
      indentMenuItem.setMnemonic('i');
      indentMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, MENU_MASK | java.awt.Event.ALT_MASK));
      outdentMenuItem.setMnemonic('o');
      outdentMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, MENU_MASK | java.awt.Event.ALT_MASK));
      expandAllMenuItem.setMnemonic('x');
      expandAllMenuItem.setAccelerator(KeyStroke.getKeyStroke('=', MENU_MASK));
      collapseAllMenuItem.setMnemonic('l');
      collapseAllMenuItem.setAccelerator(KeyStroke.getKeyStroke('-', MENU_MASK));
      deleteMenuItem.setMnemonic('k');
      deleteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, MENU_MASK));

      undoMenuItem.setMnemonic('u');
      undoMenuItem.setAccelerator(KeyStroke.getKeyStroke('Z', MENU_MASK));
      redoMenuItem.setMnemonic('r');
      redoMenuItem.setAccelerator(KeyStroke.getKeyStroke('Z', MENU_MASK | java.awt.event.InputEvent.SHIFT_MASK));
      addAboveMenuItem.setMnemonic('a');
      addAboveMenuItem.setAccelerator(KeyStroke.getKeyStroke('T', MENU_MASK));
      addBelowMenuItem.setMnemonic('b');
      addBelowMenuItem.setAccelerator(KeyStroke.getKeyStroke('B', MENU_MASK));
      addChildMenuItem.setMnemonic('c');
      addChildMenuItem.setAccelerator(KeyStroke.getKeyStroke('D', MENU_MASK));
      newFromClipboardMenuItem.setAccelerator(KeyStroke.getKeyStroke('M', MENU_MASK));
      upMenuItem.setMnemonic('u');
      upMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_UP, MENU_MASK | java.awt.Event.ALT_MASK));
      downMenuItem.setMnemonic('d');
      downMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, MENU_MASK | java.awt.Event.ALT_MASK));
      indentMenuItem.setMnemonic('i');
      indentMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, MENU_MASK | java.awt.Event.ALT_MASK));
      outdentMenuItem.setMnemonic('o');
      outdentMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, MENU_MASK | java.awt.Event.ALT_MASK));
      expandAllMenuItem.setMnemonic('x');
      expandAllMenuItem.setAccelerator(KeyStroke.getKeyStroke('=', MENU_MASK));
      collapseAllMenuItem.setMnemonic('l');
      collapseAllMenuItem.setAccelerator(KeyStroke.getKeyStroke('-', MENU_MASK));
      deleteMenuItem.setMnemonic('k');
      deleteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, MENU_MASK));



      return editMenu;
  }

  /**
   * Creates the Actions menu.
   */
  private JMenu createActionsMenu()
  {
    JMenu actionsMenu = new JMenu("Actions"); // "Actions");
    actionsMenu.setMnemonic(KeyEvent.VK_T);

    JMenuItem searchMenuItem = new JMenuItem("Search"); //"Search");
    searchMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { searchDialog.open(); }});
    actionsMenu.add(searchMenuItem);

    JMenuItem searchArticleMenuItem = new JMenuItem("Search Article"); //"Search Article");
    searchArticleMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.searchArticle(); }});
    actionsMenu.add(searchArticleMenuItem);

    JMenuItem findNextMenuItem = new JMenuItem("Find Next"); //"Find Next");
    findNextMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.findNext(); }});
    actionsMenu.add(findNextMenuItem);

    JMenuItem replaceTextMenuItem = new JMenuItem("Replace Text"); //"Replace Text");
    replaceTextMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.replaceText(); }});
    actionsMenu.add(replaceTextMenuItem);

    JMenuItem uppercaseMenuItem = new JMenuItem("Make Upper Case");
    uppercaseMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.toUpperCase(); }});
    actionsMenu.add(uppercaseMenuItem);

    JMenuItem listMenuItem = new JMenuItem("Make List");
    listMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.toList(); }});
    actionsMenu.add(listMenuItem);

    JMenuItem lowercaseMenuItem = new JMenuItem("Make Lower Case"); //"Replace Text");
    lowercaseMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.toLowerCase(); }});
    actionsMenu.add(lowercaseMenuItem);

    JMenuItem addDividerMenuItem = new JMenuItem("Add Divider"); //"Replace Text");
    addDividerMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.addDivider(); }});
    actionsMenu.add(addDividerMenuItem);

    JMenuItem launchUrlMenuItem = new JMenuItem("Follow Link"); //"Follow highlighted link");
    launchUrlMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.openURLSelectedInArticle(); }});
    actionsMenu.add(launchUrlMenuItem);

    JMenuItem formatTableMenuItem = new JMenuItem("Format Tables"); //"Format Tables");
    formatTableMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.formatTables(); }});
    actionsMenu.add(formatTableMenuItem);

    webSearchMenuItem = new JMenuItem(getPrefs().webSearchName);
    webSearchMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.webSearchTextSelectedInArticle(); }});
    actionsMenu.add(webSearchMenuItem);

    actionsMenu.add(new JSeparator());

    characterWrapArticleMenuItem = new JMenuItem("Hard Wrap Selected Text" + " " + getPrefs().characterWrapWidth + " " + ""); //);
//    characterWrapArticleMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { wrapContentToCharWidth(); }});
    characterWrapArticleMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.wrapSelectionToCharWidth(); }});
    actionsMenu.add(characterWrapArticleMenuItem);

    JMenuItem stripTagsMenuItem = new JMenuItem("Strip Tags"); //"Strip <tags> from article");
    stripTagsMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { stripAllTags(); }});
    actionsMenu.add(stripTagsMenuItem);

    JMenuItem insertDateMenuItem = new JMenuItem("Insert Date"); //"Insert date");
    insertDateMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { insertDate(); }});
    actionsMenu.add(insertDateMenuItem);

    /*
    actionsMenu.add(new JSeparator());

    JMenuItem sortMenuItem = new JMenuItem("Sort One Level"); //"Sort children (one level)");
    sortMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.sortChildren(); updateWindowTitle(); }});
    actionsMenu.add(sortMenuItem);

    JMenuItem sortRecursiveMenuItem = new JMenuItem("Sort All Levels"); //"Sort children (all levels)");
    sortRecursiveMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { theJreepad.sortChildrenRecursive(); updateWindowTitle(); }});
    actionsMenu.add(sortRecursiveMenuItem);
    */

    searchMenuItem.setMnemonic('s');
    searchMenuItem.setAccelerator(KeyStroke.getKeyStroke('F', MENU_MASK | java.awt.Event.SHIFT_MASK));
    searchArticleMenuItem.setMnemonic('a');
    searchArticleMenuItem.setAccelerator(KeyStroke.getKeyStroke('F', MENU_MASK));
    findNextMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
    replaceTextMenuItem.setMnemonic('r');
    replaceTextMenuItem.setAccelerator(KeyStroke.getKeyStroke('R', MENU_MASK));
    uppercaseMenuItem.setMnemonic('u');
    uppercaseMenuItem.setAccelerator(KeyStroke.getKeyStroke('U', MENU_MASK));
    listMenuItem.setMnemonic('u');
    listMenuItem.setAccelerator(KeyStroke.getKeyStroke('L', MENU_MASK));
    lowercaseMenuItem.setMnemonic('l');
    lowercaseMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, MENU_MASK | KeyEvent.VK_CONTROL | java.awt.Event.SHIFT_MASK));
    addDividerMenuItem.setMnemonic('d');
    addDividerMenuItem.setAccelerator(KeyStroke.getKeyStroke('H', MENU_MASK | java.awt.Event.SHIFT_MASK));
    webSearchMenuItem.setMnemonic('g');
    webSearchMenuItem.setAccelerator(KeyStroke.getKeyStroke('G', MENU_MASK));
    characterWrapArticleMenuItem.setAccelerator(KeyStroke.getKeyStroke('W', MENU_MASK));
    characterWrapArticleMenuItem.setMnemonic('w');
    launchUrlMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, MENU_MASK | KeyEvent.VK_CONTROL | java.awt.Event.ALT_MASK));
    launchUrlMenuItem.setMnemonic('k');
    stripTagsMenuItem.setAccelerator(KeyStroke.getKeyStroke('T', MENU_MASK));
    stripTagsMenuItem.setMnemonic('t');
    insertDateMenuItem.setAccelerator(KeyStroke.getKeyStroke('E', MENU_MASK));
    insertDateMenuItem.setMnemonic('e');
    formatTableMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, MENU_MASK | KeyEvent.VK_CONTROL | java.awt.Event.SHIFT_MASK));
    formatTableMenuItem.setMnemonic('b');

    return actionsMenu;
  }

  /**
   * Creates the View menu.
   */
  private JMenu createViewMenu()
  {
    JMenu viewMenu = new JMenu("View"); //"View");
    viewMenu.setMnemonic('V');

    JMenuItem viewBothMenuItem = new JMenuItem("View Both"); //"Both tree and article");
    viewBothMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { setViewMode(JreepadPrefs.VIEW_BOTH); }});
    viewMenu.add(viewBothMenuItem);
    JMenuItem viewTreeMenuItem = new JMenuItem("View Tree"); //"Tree");
    viewTreeMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { setViewMode(JreepadPrefs.VIEW_TREE); }});
    viewMenu.add(viewTreeMenuItem);
    JMenuItem viewArticleMenuItem = new JMenuItem("View Article"); //"Article");
    viewArticleMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { setViewMode(JreepadPrefs.VIEW_ARTICLE); }});
    viewMenu.add(viewArticleMenuItem);
    viewMenu.add(new JSeparator());

    JMenu viewToolbarMenu = new JMenu("Toolbar"); //"Toolbar");
    viewMenu.add(viewToolbarMenu);
      viewToolbarIconsMenuItem = new JCheckBoxMenuItem("Icons"); //"Icons", true);
      viewToolbarIconsMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
                                                  setToolbarMode(JreepadPrefs.TOOLBAR_ICON);
                                                  }});
      viewToolbarMenu.add(viewToolbarIconsMenuItem);
      viewToolbarTextMenuItem = new JCheckBoxMenuItem("Text"); //"Text", true);
      viewToolbarTextMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
                                                  setToolbarMode(JreepadPrefs.TOOLBAR_TEXT);
                                                  }});
      viewToolbarMenu.add(viewToolbarTextMenuItem);
      viewToolbarOffMenuItem = new JCheckBoxMenuItem("Off"); //"Off", true);
      viewToolbarOffMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
                                                  setToolbarMode(JreepadPrefs.TOOLBAR_OFF);
                                                  }});
      viewToolbarMenu.add(viewToolbarOffMenuItem);



    viewMenu.add(new JSeparator());

    JMenuItem articleViewModeMenuItem = new JMenu("Format Article"); //"View this article as...");
    JMenuItem articleViewModeTextMenuItem = new JMenuItem("Format Text"); //"Text");
      articleViewModeTextMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
                theJreepad.setArticleMode(JreepadArticle.ARTICLEMODE_ORDINARY);
                updateUndoRedoMenuState();
                       }});
      articleViewModeMenuItem.add(articleViewModeTextMenuItem);
      JMenuItem articleViewModeHtmlMenuItem = new JMenuItem("Format HTML"); //"HTML");
      articleViewModeHtmlMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
                theJreepad.setArticleMode(JreepadArticle.ARTICLEMODE_HTML);
                updateUndoRedoMenuState();
                       }});
      articleViewModeMenuItem.add(articleViewModeHtmlMenuItem);
      JMenuItem articleViewModeCsvMenuItem = new JMenuItem("Format CSV"); //"Table (comma-separated data)");
      articleViewModeCsvMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
                theJreepad.setArticleMode(JreepadArticle.ARTICLEMODE_CSV);
                updateUndoRedoMenuState();
                       }});
      articleViewModeMenuItem.add(articleViewModeCsvMenuItem);
      JMenuItem articleViewModeTextileMenuItem = new JMenuItem("Format Text");
      articleViewModeTextileMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
                theJreepad.setArticleMode(JreepadArticle.ARTICLEMODE_TEXTILEHTML);
                updateUndoRedoMenuState();
                       }});
      articleViewModeMenuItem.add(articleViewModeTextileMenuItem);
    viewMenu.add(articleViewModeMenuItem);


    viewMenu.add(new JSeparator());
    JMenuItem thisNodesUrlMenuItem = new JMenuItem("Node Address"); //"\"node://\" address for current node");
    thisNodesUrlMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { getTreepadNodeUrl(); }});
    viewMenu.add(thisNodesUrlMenuItem);

    thisNodesUrlMenuItem.setMnemonic('u');
    thisNodesUrlMenuItem.setAccelerator(KeyStroke.getKeyStroke('U', MENU_MASK | java.awt.Event.SHIFT_MASK));
    thisNodesUrlMenuItem.setMnemonic('n');
    viewBothMenuItem.setMnemonic('b');
    viewTreeMenuItem.setMnemonic('t');
    viewArticleMenuItem.setMnemonic('a');
    viewBothMenuItem.setAccelerator(KeyStroke.getKeyStroke('1', MENU_MASK));
    viewTreeMenuItem.setAccelerator(KeyStroke.getKeyStroke('2', MENU_MASK));
    viewArticleMenuItem.setAccelerator(KeyStroke.getKeyStroke('3', MENU_MASK));
    viewToolbarIconsMenuItem.setAccelerator(KeyStroke.getKeyStroke('4', MENU_MASK));
    viewToolbarTextMenuItem.setAccelerator(KeyStroke.getKeyStroke('5', MENU_MASK));
    viewToolbarOffMenuItem.setAccelerator(KeyStroke.getKeyStroke('6', MENU_MASK));
    articleViewModeTextMenuItem.setAccelerator(KeyStroke.getKeyStroke('7', MENU_MASK));
    articleViewModeHtmlMenuItem.setAccelerator(KeyStroke.getKeyStroke('8', MENU_MASK));
    articleViewModeCsvMenuItem.setAccelerator(KeyStroke.getKeyStroke('9', MENU_MASK));
    articleViewModeTextileMenuItem.setAccelerator(KeyStroke.getKeyStroke('0', MENU_MASK));
    viewToolbarMenu.setMnemonic('o');

    viewMenu.add(new JSeparator());


    // section to adjust fonts
    JMenu articleFontMenu = new JMenu("Article Font");
    viewMenu.add(articleFontMenu);
    JMenuItem increaseFontMenuItem = new JMenuItem("Increase Font");
    increaseFontMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        theJreepad.changeFont(FontHelper.FONT_DIR_UP, JreepadView.CHANGE_ARTICLE_FONT);
      }
    });
    increaseFontMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS, MENU_MASK | KeyEvent.VK_CONTROL | java.awt.Event.SHIFT_MASK));
    JMenuItem decreaseFontMenuItem = new JMenuItem("Decrease Font");
    decreaseFontMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        theJreepad.changeFont(FontHelper.FONT_DIR_DOWN, JreepadView.CHANGE_ARTICLE_FONT);
      }
    });
    decreaseFontMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, MENU_MASK | KeyEvent.VK_CONTROL | java.awt.Event.SHIFT_MASK));
    articleFontMenu.add(increaseFontMenuItem);
    articleFontMenu.add(decreaseFontMenuItem);



    JMenu treeFontMenu = new JMenu("Tree Font");
    viewMenu.add(treeFontMenu);
    JMenuItem increaseTreeFontMenuItem = new JMenuItem("Increase Font");
    increaseTreeFontMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        theJreepad.changeFont(FontHelper.FONT_DIR_UP, JreepadView.CHANGE_TREE_FONT);
      }
    });
    increaseTreeFontMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS, MENU_MASK | KeyEvent.VK_CONTROL | java.awt.Event.SHIFT_MASK | java.awt.Event.ALT_MASK));
    JMenuItem decreaseTreeFontMenuItem = new JMenuItem("Decrease Font");
    decreaseTreeFontMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        theJreepad.changeFont(FontHelper.FONT_DIR_DOWN, JreepadView.CHANGE_TREE_FONT);
      }
    });
    decreaseTreeFontMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, MENU_MASK | KeyEvent.VK_CONTROL | java.awt.Event.SHIFT_MASK | java.awt.Event.ALT_MASK));
    treeFontMenu.add(increaseTreeFontMenuItem);
    treeFontMenu.add(decreaseTreeFontMenuItem);



    return viewMenu;
  }

  /**
   * Creates the Options menu.
   */
  private JMenu createOptionsMenu()
  {
      JMenu optionsMenu = new JMenu("Options"); //"Options");
      optionsMenu.setMnemonic('O');

      JMenuItem autoSaveMenuItem = new JMenuItem("AutoSave"); //"Autosave...");
      autoSaveMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) { showAutoSaveDialog(); }});
      optionsMenu.add(autoSaveMenuItem);
      JMenuItem prefsMenuItem = new JMenuItem("Preferences"); //"Preferences");
      prefsMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
                                              // updateFontsInPrefsBox();
                                              showPrefsDialog(); }});
      optionsMenu.add(prefsMenuItem);

      autoSaveMenuItem.setMnemonic('a');
      prefsMenuItem.setMnemonic('p');

      return optionsMenu;
  }

  private JMenu createHelpMenu()
  {
      JMenu helpMenu = new JMenu("Help"); //"Help");
      helpMenu.setMnemonic('H');

      JMenuItem jpHelpMenuItem = new JMenuItem("Jreepad Help"); //"Jreepad Help");
      jpHelpMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e)
            { jpHelp();
            }});
      helpMenu.add(jpHelpMenuItem);

      JMenuItem jpKeysMenuItem = new JMenuItem("Keys Help"); //"Jreepad Help");
      jpKeysMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e)
            { keyHelp();
            }});
      helpMenu.add(jpKeysMenuItem);

      JMenuItem jpLinksMenuItem = new JMenuItem("Links Help"); //"Jreepad Help");
      jpLinksMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e)
            { linkHelp();
            }});
      helpMenu.add(jpLinksMenuItem);

      /*
      JMenuItem keyboardHelpMenuItem = new JMenuItem("Keyboard Shortcuts"); //"Keyboard shortcuts");
      keyboardHelpMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e)
            { keyboardHelp();
            }});
      helpMenu.add(keyboardHelpMenuItem);
      JMenuItem linksHelpMenuItem = new JMenuItem("Help Links"); //"Help with links");
      linksHelpMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e)
            { linksHelp();
            }});
      helpMenu.add(linksHelpMenuItem);
      JMenuItem dragDropHelpMenuItem = new JMenuItem("Drag and Drop Help"); //"Help with drag-and-drop");
      dragDropHelpMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e)
            { dragDropHelp();
            }});
      helpMenu.add(dragDropHelpMenuItem);
      */
      helpMenu.add(new JSeparator());
      JMenuItem aboutMenuItem = new JMenuItem("About"); //"About Jreepad");
      aboutMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e)
            {
              aboutAction();
            }});
      aboutMenuItem.setMnemonic('a');
      helpMenu.add(aboutMenuItem);
      JMenuItem licenseMenuItem = new JMenuItem("License"); //"License");
      licenseMenuItem.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e)
              {
                showLicense();
              }});
      helpMenu.add(licenseMenuItem);

      /*
      keyboardHelpMenuItem.setMnemonic('k');
      dragDropHelpMenuItem.setMnemonic('d');
      linksHelpMenuItem.setMnemonic('l');
      */
      licenseMenuItem.setMnemonic('i');

      return helpMenu;
  }

  // Used by the constructor
  public void establishToolbar()
  {
    // Add the toolbar buttons
    toolBar = Box.createHorizontalBox();
    toolBarIconic = Box.createHorizontalBox();
    //
    addAboveButton = new JButton("Add Above");
    addBelowButton = new JButton("Add Below");
    addButton = new JButton("Add Child");
    removeButton = new JButton("Delete");
    //
    upButton = new JButton("Up");
    downButton = new JButton("Down");
    indentButton = new JButton("In");
    outdentButton = new JButton("Out");
    //
    // Now the mnemonics...
    addAboveButton.setMnemonic('a');
    addBelowButton.setMnemonic('b');
    addButton.setMnemonic('c');
    upButton.setMnemonic('u');
    downButton.setMnemonic('d');
    indentButton.setMnemonic('i');
    outdentButton.setMnemonic('o');
    removeButton.setMnemonic('k');
    //
    viewSelector = new JComboBox(new String[]{"View Both","View Tree","View Article"});
    viewSelector.setEditable(false);
    viewSelector.setSelectedIndex(0);
    viewSelector.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){
                               switch(viewSelector.getSelectedIndex())
                               {
                                 case 1:
                                   setViewMode(JreepadPrefs.VIEW_TREE); break;
                                 case 2:
                                   setViewMode(JreepadPrefs.VIEW_ARTICLE); break;
                                 default:
                                   setViewMode(JreepadPrefs.VIEW_BOTH); break;
                               }
                               } });
    viewSelectorIconic = new JComboBox(new String[]{"View Both","View Tree","View Article"});
    viewSelectorIconic.setEditable(false);
    viewSelectorIconic.setSelectedIndex(0);
    viewSelectorIconic.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){
                               switch(viewSelectorIconic.getSelectedIndex())
                               {
                                 case 1:
                                   setViewMode(JreepadPrefs.VIEW_TREE); break;
                                 case 2:
                                   setViewMode(JreepadPrefs.VIEW_ARTICLE); break;
                                 default:
                                   setViewMode(JreepadPrefs.VIEW_BOTH); break;
                               }
                               } });

    // Add the actions to the toolbar buttons
    upButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.moveCurrentNodeUp(); repaint();  theJreepad.returnFocusToTree(); updateWindowTitle();} });
    downButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.moveCurrentNodeDown(); repaint();  theJreepad.returnFocusToTree(); updateWindowTitle();} });


    indentButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.indentCurrentNode(); repaint();  theJreepad.returnFocusToTree(); updateWindowTitle();} });
    outdentButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.outdentCurrentNode(); repaint(); theJreepad.returnFocusToTree(); updateWindowTitle(); } });




    addAboveButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.addNodeAbove(); repaint(); updateWindowTitle();} });
    addBelowButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.addNodeBelow(); repaint(); updateWindowTitle();} });
    addButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.addNode(); repaint(); updateWindowTitle();} });
    removeButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ deleteNodeAction(); } });


    // Now establish the iconic buttons (code contributed by Coen Schalkwijk)
        // New file
        newIconButton = new JButton();
        newIconButton.setToolTipText("New");
        newIconButton.setBorderPainted(false);
        // newIconButton.setIcon(this.getIcon("New16.gif"));

        // Open existing
        openIconButton = new JButton();
        openIconButton.setToolTipText("Open ");
        openIconButton.setBorderPainted(false);
        // openIconButton.setIcon(this.getIcon("Open16.gif"));

        // Save current
        JButton saveIconButton = new JButton(new SaveAction());
        saveIconButton.setBorderPainted(false);
        saveIconButton.setText(null); // Ignore action text

        // Insert node before
        addAboveIconButton = new JButton("Add Above");

        // Insert node after
        addBelowIconButton = new JButton("Add Below");
        addBelowIconButton.setMnemonic('b');

        // Add child node
        addIconButton = new JButton();
        addIconButton.setMnemonic('c');
        addIconButton.setToolTipText("Add Child");
        addIconButton.setBorderPainted(false);
        // addIconButton.setIcon(this.getIcon("Add16.gif"));

        // Remove node
        removeIconButton = new JButton();
        removeIconButton.setMnemonic('k');
        removeIconButton.setToolTipText("Delete");
        removeIconButton.setBorderPainted(false);
        // removeIconButton.setIcon(this.getIcon("Remove16.gif"));

        // Move node up
        upIconButton = new JButton();
        upIconButton.setMnemonic('u');
        upIconButton.setToolTipText("Up");
        upIconButton.setBorderPainted(false);
        // upIconButton.setIcon(this.getIcon("Up16.gif"));

        // Move node down
        downIconButton = new JButton();
        downIconButton.setMnemonic('d');
        downIconButton.setToolTipText("Down");
        downIconButton.setBorderPainted(false);
        // downIconButton.setIcon(this.getIcon("Down16.gif"));

        // Move node from current
        outdentIconButton = new JButton();
        outdentIconButton.setMnemonic('i');
        outdentIconButton.setToolTipText("In");
        outdentIconButton.setBorderPainted(false);
        // outdentIconButton.setIcon(this.getIcon("Back16.gif"));

        // Move node to previous
        indentIconButton = new JButton();
        indentIconButton.setMnemonic('o');
        indentIconButton.setToolTipText("Out");
        indentIconButton.setBorderPainted(false);
        // indentIconButton.setIcon(this.getIcon("Forward16.gif"));

    // Add the actions to the toolbar buttons
    newIconButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ newAction(); } });
    openIconButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ openAction(); } });
    upIconButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.moveCurrentNodeUp(); repaint();
          theJreepad.returnFocusToTree();
          updateWindowTitle();} });
    downIconButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.moveCurrentNodeDown(); repaint();
          theJreepad.returnFocusToTree();
          updateWindowTitle();} });
    indentIconButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.indentCurrentNode(); repaint();
          theJreepad.returnFocusToTree();
          updateWindowTitle();} });
    outdentIconButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.outdentCurrentNode(); repaint();
          theJreepad.returnFocusToTree();
          updateWindowTitle(); } });
    addAboveIconButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.addNodeAbove(); repaint(); /*
          theJreepad.returnFocusToTree(); */
          updateWindowTitle();} });
    addBelowIconButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.addNodeBelow(); repaint(); /*
          theJreepad.returnFocusToTree(); */
          updateWindowTitle();} });
    addIconButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ theJreepad.addNode(); repaint(); /*
          theJreepad.returnFocusToTree(); */
          updateWindowTitle();} });
    removeIconButton.addActionListener(new ActionListener(){
                               public void actionPerformed(ActionEvent e){ deleteNodeAction(); } });
    // Finished establishing the iconic buttons




// Add all the buttons to their respective toolbar
    toolBar.add(addAboveButton);
    toolBar.add(addBelowButton);
    toolBar.add(addButton);
    toolBar.add(removeButton);
    toolBar.add(upButton);
    toolBar.add(downButton);
    toolBar.add(indentButton);
    toolBar.add(outdentButton);
    toolBar.add(viewSelector);
    toolBar.add(Box.createGlue());


    toolBarIconic.add(newIconButton);
    toolBarIconic.add(openIconButton);
    toolBarIconic.add(saveIconButton);
    toolBarIconic.add(addAboveIconButton);
    toolBarIconic.add(addBelowIconButton);
    toolBarIconic.add(addIconButton);
    toolBarIconic.add(removeIconButton);
    toolBarIconic.add(upIconButton);
    toolBarIconic.add(downIconButton);
    toolBarIconic.add(outdentIconButton);
    toolBarIconic.add(indentIconButton);
    toolBarIconic.add(viewSelectorIconic);
    toolBarIconic.add(Box.createGlue());
  }


  private ClassLoader loader = this.getClass().getClassLoader();
  private final Icon getIcon(String name){
    try{
       // TODO create single funct for all img loading
       URL iconUrl = loader.getResource("images/"+name);
         return new ImageIcon(iconUrl);
       }catch(Exception e){
         System.err.println("Jreepad: Icon image failed to load: images/"+name);
         // e.printStackTrace();// Ignore, use default icon
       }
     return null;
  }



  protected void setToolbarMode(int newMode)
  {
    switch(newMode)
    {
      case JreepadPrefs.TOOLBAR_TEXT:
        toolBar.setVisible(true);
        toolBarIconic.setVisible(false);
        break;
      case JreepadPrefs.TOOLBAR_ICON:
        toolBar.setVisible(false);
        toolBarIconic.setVisible(true);
        break;
      case JreepadPrefs.TOOLBAR_OFF:
        toolBar.setVisible(false);
        toolBarIconic.setVisible(false);
        break;
      default:
        // Invalid mode passed
        return;
    }
    getPrefs().toolbarMode = newMode;
    viewToolbarIconsMenuItem.setSelected(newMode==JreepadPrefs.TOOLBAR_ICON);
    viewToolbarTextMenuItem.setSelected(newMode==JreepadPrefs.TOOLBAR_TEXT);
    viewToolbarOffMenuItem.setSelected(newMode==JreepadPrefs.TOOLBAR_OFF);
    repaint();
  }

  public void establishAutosaveDialogue()
  {
    Box vBox, hBox;
    // Establish the autosave dialogue box
    autoSaveDialog = new JDialog(this, "Autosave", true);
    autoSaveDialog.setVisible(false);
    vBox = Box.createVerticalBox();
    vBox.add(Box.createGlue());
    hBox = Box.createHorizontalBox();
    hBox.add(autoSaveCheckBox = new JCheckBox("Every", getPrefs().autoSave));
    hBox.add(autoSavePeriodSpinner = new JSpinner(new SpinnerNumberModel(getPrefs().autoSavePeriod, 1, 1000, 1)));
    hBox.add(new JLabel("minutes"));
    vBox.add(hBox);
    vBox.add(Box.createGlue());
    hBox = Box.createHorizontalBox();
    hBox.add(autoSaveOkButton = new JButton("Ok"));
    hBox.add(autoSaveCancelButton = new JButton("Cancel"));
    autoSaveOkButton.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){
                                    getPrefs().autoSavePeriod = ((Integer)(autoSavePeriodSpinner.getValue())).intValue();
                                    getPrefs().autoSave = autoSaveCheckBox.isSelected();
                                    autoSaveDialog.setVisible(false);
                                    if(getPrefs().autoSave && !(autoSaveThread.isAlive()))
                                    {
                                      autoSaveWarningMessage();
                                        autoSaveThread.start();
                                    }
                                    updateWindowTitle();
                                   }});
    autoSaveCancelButton.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){autoSaveDialog.setVisible(false);}});
    vBox.add(Box.createGlue());
    vBox.add(hBox);
    autoSaveDialog.getContentPane().add(vBox);
    // Finished establishing the autosave dialogue box
  }

  public void autoSaveWarningMessage()
  {
      JOptionPane.showMessageDialog(this, "Long?", "Active" , JOptionPane.INFORMATION_MESSAGE);
  }

  public void establishNodeUrlDisplayDialogue()
  {
    Box vBox;
    // Establish the nodeUrlDisplay dialogue box
    nodeUrlDisplayDialog = new JDialog(this, "Node URL", true);
    nodeUrlDisplayDialog.setVisible(false);
    vBox = Box.createVerticalBox();
    vBox.add(new JLabel("Node Url"));
    vBox.add(nodeUrlDisplayField = new JTextField("node://its/a/secret"));
    vBox.add(new JLabel("Node Url"));
    vBox.add(nodeUrlDisplayOkButton = new JButton("Ok"));
    nodeUrlDisplayOkButton.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){
                                    nodeUrlDisplayDialog.setVisible(false);
                                   }});
    nodeUrlDisplayDialog.getContentPane().add(vBox);
    nodeUrlDisplayField.requestFocus();
    // Finished: Establish the nodeUrlDisplay dialogue box
  }

  public static void main(String[] args)
  {

    // System.err.println("" + args.length + " input arguments provided.");
    // for(int i=0; i<args.length; i++){
    //   System.err.println(args[i]);
    // }

    try
    {
      UIManager.setLookAndFeel(
      UIManager.getSystemLookAndFeelClassName());
    } catch (Exception e)
    {}

    String launchFilename="", launchPrefsFilename=null;

    int ARGMODE_FILE = 0;
    int ARGMODE_PREF = 1;
    int argMode = ARGMODE_FILE;

    for(int i=0; i<args.length; i++)
    {
      if(args[i].startsWith("-h") || args[i].startsWith("--h"))
      {
        System.out.println("Jreepad command-line arguments:");
        System.out.println("  -p [prefsfile]    Load/save preferences from named location instead of default");
        System.out.println("  [filename]        Jreepad/treepad file to load on startup");
        System.out.println("  ");
        System.out.println("For example:");
        System.out.println("  java -jar Jreepad.jar -p /Users/jo/Library/jreeprefs.dat /Users/jo/Documents/mynotes.hjt");
        System.exit(1);
      }
      else if(args[i].equals("-p"))
        argMode = ARGMODE_PREF;
      else if(argMode == ARGMODE_PREF && launchPrefsFilename==null)
      {
        launchPrefsFilename=args[i];
        argMode = ARGMODE_FILE;
      }
      else if(argMode == ARGMODE_FILE && launchFilename.equals(""))
      {
        launchFilename=args[i];
      }
    }

    // System.err.println("Launching using prefs file \"" + launchPrefsFilename + "\" and loadfile \"" + launchFilename + "\"\n");

    new JreepadViewer(launchFilename, launchPrefsFilename);

    /*
    The old way of handling the arguments

    if(args.length==0)
      new JreepadViewer();
    else if(args.length==1)
      new JreepadViewer(args[0]);
    else
      System.err.println("Only one (optional) argument can be passed - the name of the HJT file to load.");
    */

  }

  /**
   * Ask the user whether to save the current file if it is unsaved.
   * @param prompt  message to display
   * @return true if the user clicked "Yes" and the save was successful or the user clicked "No" or the file didn't need to be saved;
   *         false if the user clicked "Cancel" or the save failed
   */
  private boolean askAndSave(String prompt)
  {
    if (document.isContentSaved())
      return true;
    int answer = JOptionPane.showConfirmDialog(this, prompt,
                       "Save?" , JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
    if(answer == JOptionPane.CANCEL_OPTION)
      return false;
    if(answer == JOptionPane.YES_OPTION)
    {
      SaveAction saveAction = new SaveAction();
      saveAction.actionPerformed(null);
      if (!saveAction.isSuccessful())
        return false;
    }
    return true;
  }

  private void newAction()
  {
    if (!askAndSave("Save current file first?"))
      return;

    content.remove(theJreepad);
    document = new JreepadTreeModel();
    theJreepad = new JreepadView(document);
    content.add(theJreepad);
    searchDialog.setJreepad(theJreepad);

    setTitleBasedOnFilename("");
    validate();
    repaint();
    document.setContentSaved(true);
    updateUndoRedoMenuState();
//  theJreepad.clearUndoCache();
  }

  private void openAction()
  {
    if (!askAndSave("Save current file before opening?"))
      return;

    fileChooser.setCurrentDirectory(getPrefs().openLocation);
    if(fileChooser.showOpenDialog(this) != JFileChooser.APPROVE_OPTION)
        return;

    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    openFile(fileChooser.getSelectedFile());
    addToOpenRecentMenu(fileChooser.getSelectedFile());
    savePreferencesFile();
    setCursor(Cursor.getDefaultCursor());
    updateUndoRedoMenuState();
  } // End of: openAction()

  public boolean openFile(File file)
  {
    addToOpenRecentMenu(file);
    savePreferencesFile();

    getPrefs().openLocation = file; // Remember the open directory
    try
    {
      InputStream in = new FileInputStream(file);
      JreepadReader reader = new AutoDetectReader(getPrefs().getEncoding(), getPrefs().autoDetectHtmlArticles);
      document = reader.read(in);
      document.setSaveLocation(file);
    }
    catch(IOException e)
    {
      JOptionPane.showMessageDialog(this, e, "File load failed" , JOptionPane.ERROR_MESSAGE);
      return false;
    }
    content.remove(theJreepad);
    theJreepad = new JreepadView(document);
    content.add(theJreepad);
    searchDialog.setJreepad(theJreepad);

    getPrefs().exportLocation = getPrefs().importLocation = file;
    setTitleBasedOnFilename(file.getName());

    addToOpenRecentMenu(file);
    savePreferencesFile();

    validate();
    repaint();
    return true;
  }

  private class SaveAction extends AbstractAction
  {
    private boolean askForFilename;

    private boolean successful = false;

    public SaveAction()
    {
        this(false);
    }

    public SaveAction(boolean askForFilename)
    {
        super(askForFilename ? "Save As" : "Save");
        // super(askForFilename ? "Save As" : "Save", getIcon("Save16.gif"));
        this.askForFilename = askForFilename;

        if (askForFilename)
        {
            putValue(MNEMONIC_KEY, new Integer(KeyEvent.VK_A));
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke('A', MENU_MASK));
        }
        else
        {
            putValue(MNEMONIC_KEY, new Integer(KeyEvent.VK_S));
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke('S', MENU_MASK));
            putValue(SHORT_DESCRIPTION, "Save");
        }
    }

    public void actionPerformed(ActionEvent e)
    {
        int fileType = getPrefs().mainFileType;
        if (document.getSaveLocation() != null)
            fileType = document.getFileType();

        File saveLocation = document.getSaveLocation();
        if(askForFilename || saveLocation==null || (saveLocation.isFile() && !saveLocation.canWrite()))
        {
          // Ask for filename
          SaveFileChooser fileChooser = new SaveFileChooser(getPrefs().mainFileType);
          fileChooser.setCurrentDirectory(getPrefs().openLocation);

          fileChooser.setSelectedFile(new File(document.getRootNode().getTitle() +
                       (fileType==JreepadPrefs.FILETYPE_XML?".jree":".hjt")));
          if(fileChooser.showSaveDialog(JreepadViewer.this) != JFileChooser.APPROVE_OPTION)
          {
              successful = false;
              return; // No file chosen
          }
          saveLocation = fileChooser.getSelectedFile();
          fileType = fileChooser.getFileType();
        }
        getPrefs().openLocation = saveLocation; // Remember the file's directory

        // Save the file
        try
        {
          setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

          // Select default application encoding or document encoding
          String encoding;
          if (askForFilename || document.getEncoding() == null)
            encoding = getPrefs().getEncoding();
          else
            encoding = document.getEncoding();

          // Write to either HJT or XML
          JreepadWriter writer;
          if(fileType == JreepadPrefs.FILETYPE_XML)
            writer = new XmlWriter();
          else if (fileType == JreepadPrefs.FILETYPE_XML_ENCRYPTED)
          {
              String password = document.getPassword();
              if (password == null || askForFilename)
                  password = PasswordDialog.showPasswordDialog(true);
              if (password == null)
              {
                  successful = false; // "Cancel" was pressed
                  return;
              }
              EncryptedWriter encryptedWriter = new EncryptedWriter(new XmlWriter());
              encryptedWriter.setPassword(password);
              writer = encryptedWriter;
              document.setPassword(password);
          }
          else
            writer = new TreepadWriter(encoding);

          OutputStream fos = new FileOutputStream(saveLocation);
          writer.write(fos, document);
          fos.close();

          // When calling "Save As..." remember the _new_ file settings
          document.setSaveLocation(saveLocation);
          document.setFileType(fileType);
          document.setEncoding(encoding);

          updateWindowTitle();
          addToOpenRecentMenu(saveLocation);
          savePreferencesFile();
          setCursor(Cursor.getDefaultCursor());
          successful = true;
        }
        catch(IOException err)
        {
          setCursor(Cursor.getDefaultCursor());
          JOptionPane.showMessageDialog(JreepadViewer.this, err, "File Error" , JOptionPane.ERROR_MESSAGE);
          successful = false;
        }
    }

    public boolean isSuccessful()
    {
        return successful;
    }
  }

  private boolean backupToAction()
  {
    try
    {
      fileChooser.setCurrentDirectory(getPrefs().backupLocation);
      if(fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION && checkOverwrite(fileChooser.getSelectedFile()))
      {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        getPrefs().backupLocation = fileChooser.getSelectedFile();

        // Write to either HJT
        JreepadWriter writer = new TreepadWriter(getPrefs().getEncoding());
        OutputStream fos = new FileOutputStream(getPrefs().backupLocation);
        writer.write(fos, document);
        fos.close();

        setCursor(Cursor.getDefaultCursor());
        return true;
      }
      else
        return false;
    }
    catch(IOException err)
    {
      setCursor(Cursor.getDefaultCursor());
      JOptionPane.showMessageDialog(this, err, "File Error" , JOptionPane.ERROR_MESSAGE);
    }
    return false;
  } // End of: backupToAction()


  private void setTitleBasedOnFilename(String filename)
  {
    if(filename=="")
    {
      windowTitle = "Jreepad (Java Treepad Editor)";
    }
    else
      windowTitle = filename + " - Jreepad";
    updateWindowTitle();
  }

  private void updateWindowTitle()
  {
    setTitle(windowTitle + (document.isContentSaved()?"":"*") + (getPrefs().autoSave?" ["+"Autosave Active"+"]":""));
  }

  private static final int FILE_FORMAT_HJT=1;
  private static final int FILE_FORMAT_HTML=2;
  private static final int FILE_FORMAT_XML=3;
  private static final int FILE_FORMAT_TEXT=4;
  private static final int FILE_FORMAT_TEXTASLIST=5;
  private static final int FILE_FORMAT_ARTICLESTOTEXT=6;
  private void importAction(int importFormat)
  {
    boolean multipleFiles;
    try
    {

      multipleFiles = (importFormat == FILE_FORMAT_TEXT);

      if(multipleFiles)
        fileChooser.setMultiSelectionEnabled(true);
      fileChooser.setCurrentDirectory(getPrefs().importLocation);
      fileChooser.setSelectedFile(new File(theJreepad.getCurrentNode().getTitle()));

      if(fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
      {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        getPrefs().importLocation = fileChooser.getSelectedFile();

        switch(importFormat)
        {
          case FILE_FORMAT_HJT:
            InputStream in = new FileInputStream(getPrefs().importLocation);
            JreepadReader reader = new AutoDetectReader(getPrefs().getEncoding(), getPrefs().autoDetectHtmlArticles);
            theJreepad.addChild(reader.read(in).getRootNode());
            break;
          case FILE_FORMAT_TEXT:
            theJreepad.addChildrenFromTextFiles(fileChooser.getSelectedFiles());
            break;
          case FILE_FORMAT_TEXTASLIST:
            theJreepad.addChildrenFromListTextFile(new InputStreamReader(new FileInputStream(getPrefs().importLocation), getPrefs().getEncoding()));
            break;
          default:
            setCursor(Cursor.getDefaultCursor());
            JOptionPane.showMessageDialog(this,  "Unknown which format to import - coding error! Oops!","ERROR" , JOptionPane.ERROR_MESSAGE);
            return;
        }
        updateWindowTitle();
      }
      fileChooser.setMultiSelectionEnabled(false);
      setCursor(Cursor.getDefaultCursor());
    }
    catch(IOException err)
    {
      setCursor(Cursor.getDefaultCursor());
      JOptionPane.showMessageDialog(this, err, "ERROR" , JOptionPane.ERROR_MESSAGE);
    }
  } // End of: importAction()

  private void exportAction(int exportFormat)
  {
    try
    {
      fileChooser.setCurrentDirectory(getPrefs().exportLocation);
      String suggestFilename = theJreepad.getCurrentNode().getTitle();
      switch(exportFormat)
      {
        case FILE_FORMAT_HJT:
          suggestFilename += ".hjt";
          break;
        case FILE_FORMAT_HTML:
          suggestFilename += ".html";
          break;
        case FILE_FORMAT_XML:
          suggestFilename += ".xml";
          break;
        case FILE_FORMAT_TEXT:
        case FILE_FORMAT_TEXTASLIST:
        case FILE_FORMAT_ARTICLESTOTEXT:
          suggestFilename += ".txt";
          break;
      }
      fileChooser.setSelectedFile(new File(suggestFilename));
      if(fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION && checkOverwrite(fileChooser.getSelectedFile()))
      {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        getPrefs().exportLocation = fileChooser.getSelectedFile();

        String output = null;
        JreepadWriter writer = null;
        switch(exportFormat)
        {
          case FILE_FORMAT_HTML:
            writer = new HtmlWriter(getPrefs().getEncoding(),
                getPrefs().htmlExportArticleType,
                getPrefs().htmlExportUrlsToLinks,
                getPrefs().htmlExportAnchorLinkType);
            break;
          case FILE_FORMAT_XML:
            writer= new XmlWriter();
            break;
          case FILE_FORMAT_HJT:
            writer= new TreepadWriter(getPrefs().getEncoding());
            break;
          case FILE_FORMAT_TEXT:
            output = theJreepad.getCurrentNode().getContent();
            break;
          case FILE_FORMAT_TEXTASLIST:
            output = theJreepad.getCurrentNode().exportTitlesAsList();
            break;
          case FILE_FORMAT_ARTICLESTOTEXT:
            int answer = JOptionPane.showConfirmDialog(this, "Format Text?",
                       "Format" , JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            boolean titlesToo = (answer == JOptionPane.YES_OPTION);
            output = theJreepad.getCurrentNode().exportArticlesToText(titlesToo);
            break;
          default:
            setCursor(Cursor.getDefaultCursor());
            JOptionPane.showMessageDialog(this,  "Unknown which format to export - coding error! Oops!","ERROR" , JOptionPane.ERROR_MESSAGE);
            return;
        }

        OutputStream fos = new FileOutputStream(getPrefs().exportLocation);
        if (writer != null)
        {
          writer.write(fos, new JreepadTreeModel(theJreepad.getCurrentNode()));
          fos.close();
        }
        else // assume (output != null)
        {
          OutputStreamWriter osw = new OutputStreamWriter(fos, getPrefs().getEncoding());
          osw.write(output);
          osw.close();
        }
        setCursor(Cursor.getDefaultCursor());
      }
    }
    catch(IOException err)
    {
      setCursor(Cursor.getDefaultCursor());
      JOptionPane.showMessageDialog(this, err, "Cannot open file" , JOptionPane.ERROR_MESSAGE);
    }
  } // End of: exportAction()

  private void subtreeToBrowserForPrintAction()
  {
    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

    HtmlWriter writer = new HtmlWriter(
        getPrefs().getEncoding(),
        getPrefs().htmlExportArticleType,
        getPrefs().htmlExportUrlsToLinks,
        getPrefs().htmlExportAnchorLinkType,
        true);
    String output = writer.write(theJreepad.getCurrentNode());

    toBrowserForPrintAction(output);

    setCursor(Cursor.getDefaultCursor());
  } // End of: subtreeToBrowserForPrintAction()

  private void articleToBrowserForPrintAction()
  {
    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

    String output = theJreepad.getCurrentNode().getArticle().exportAsHtml(
      getPrefs().htmlExportArticleType, getPrefs().htmlExportUrlsToLinks,
      getPrefs().htmlExportAnchorLinkType, true);

    toBrowserForPrintAction(output);
    setCursor(Cursor.getDefaultCursor());
  } // End of: articleToBrowserForPrintAction()

  private void toBrowserForPrintAction(String output)
  {
    String surl = "";
    try
    {
      File systemTempFile = File.createTempFile("TMPjree", ".html");

      FileOutputStream fO = new FileOutputStream(systemTempFile);
      DataOutputStream dO = new DataOutputStream(fO);
      dO.writeBytes(output);
      dO.close();
      fO.close();
      setCursor(Cursor.getDefaultCursor());

      // Now launch the file in a browser...
      surl = systemTempFile.toURL().toString();
      new BrowserLauncher(null).openURLinBrowser(surl);
    }
    catch (BrowserLaunchingInitializingException e)
    {
      displayBrowserLauncherException(e, surl);
    }
    catch (BrowserLaunchingExecutionException e)
    {
      displayBrowserLauncherException(e, surl);
    }
    catch (UnsupportedOperatingSystemException e)
    {
      displayBrowserLauncherException(e, surl);
    }
    catch (IOException err)
    {
      JOptionPane.showMessageDialog(this, err, "File error",
        JOptionPane.ERROR_MESSAGE);
    }
  }

  private void displayBrowserLauncherException(Exception e, String url)
  {
    JOptionPane.showMessageDialog(this, "Error while opening URL:\n" + url + "\n"
      + e.getMessage() + "\n\n"
      + "The \"BrowserLauncher\" used to open a URL is an open-source Java library \n"
      + "separate from Jreepad itself - i.e. a separate Sourceforge project. \n"
      + "It may be a good idea to submit a bug report to\n"
      + "http://browserlaunch2.sourceforge.net/\n\n"
      + "If you do, please remember to supply information about the operating system\n"
      + "you are using - which type, and which version.", "ERROR",
      JOptionPane.ERROR_MESSAGE);
  }


  public void quitAction()
  {
    // We need to check about warning-if-unsaved!

    // For a multiple-Jreepad interface, we would need to use:
    //  for(int i=0; i<theApps.length(); i++)
    //  {
    //    currApp = getApp(i);
    //    if(currApp.warnAboutUnsaved())
    //    {
    //

    if (!askAndSave("Save before exiting?"))
      return;

    // Save preferences - including window position and size, and open/closed state of the current tree's nodes
    getPrefs().treePathCollection = new TreePathCollection(theJreepad.getAllExpandedPaths());
    getPrefs().windowLeft = getX();
    getPrefs().windowTop = getY();
    getPrefs().windowWidth = getWidth();
    getPrefs().windowHeight = getHeight();
    savePreferencesFile();

    System.exit(0);
  }

  private void savePreferencesFile()
  {
    getPrefs().save();
    /*
    try
    {
      ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(prefsFile));
      out.writeObject(getPrefs());
      out.close();
    }
    catch(IOException err)
    {
    }
    */
  }

  private void setViewMode(int mode)
  {
    theJreepad.setViewMode(mode);
    // Update the dropdown menu
    if(mode==JreepadPrefs.VIEW_ARTICLE){
      viewSelector.setSelectedIndex(2);
    }else if(mode==JreepadPrefs.VIEW_TREE){
      viewSelector.setSelectedIndex(1);
    }else{
      viewSelector.setSelectedIndex(0);
    }

    // Update the preferences object
    getPrefs().viewWhich = mode;

    // Update the undo menus
    updateUndoRedoMenuState();
  }

  private static JreepadPrefs getPrefs() { return JreepadView.getPrefs(); }
  private static void setPrefs(JreepadPrefs thesePrefs) { JreepadView.setPrefs(thesePrefs); }

  private void setViewToolbar(boolean boo)
  {
    toolBar.setVisible(boo);
  }

  private void undoAction()
  {
    UndoManager undoMgr = theJreepad.getCurrentNode().getArticle().getUndoMgr();
    String undoStyle = undoMgr.getUndoPresentationName();
    try{
      // This "while" should roll multiple adds or deletes into one.
      //System.out.println(undoStyle);
      //System.out.println(undoMgr.getUndoPresentationName());
      while(undoStyle.equals(undoMgr.getUndoPresentationName()))
        undoMgr.undo();
    }catch(CannotUndoException ex){
    }
    updateWindowTitle();
    updateUndoRedoMenuState();
  }
  private void redoAction(){
    UndoManager undoMgr = theJreepad.getCurrentNode().getArticle().getUndoMgr();
    String redoStyle = undoMgr.getRedoPresentationName();
    try{
      while(redoStyle.equals(undoMgr.getRedoPresentationName()))
        undoMgr.redo();
    }catch(CannotRedoException ex){
    }
    updateWindowTitle();
    updateUndoRedoMenuState();
  }

  public void updateUndoRedoMenuState(){
    undoMenuItem.setEnabled(isArticleUndoPossible());
    redoMenuItem.setEnabled(isArticleRedoPossible());
  }

  public boolean isArticleUndoPossible(){
    if(getPrefs().viewWhich == JreepadPrefs.VIEW_TREE)
      return false;

    if(theJreepad.getCurrentNode().getArticle().getArticleMode() != JreepadArticle.ARTICLEMODE_ORDINARY)
      return false;

    // Deactivated: since this class can't tell if text has been typed or not...
    // if(!theJreepad.getCurrentNode().undoMgr.canUndo())
    //   return false;

    return true;
  }
  public boolean isArticleRedoPossible(){
    if(getPrefs().viewWhich == JreepadPrefs.VIEW_TREE)
      return false;

    if(theJreepad.getCurrentNode().getArticle().getArticleMode() != JreepadArticle.ARTICLEMODE_ORDINARY)
      return false;

    if(!theJreepad.getCurrentNode().getArticle().getUndoMgr().canRedo())
      return false;

    return true;
  }

  private void aboutAction()
  {
              JOptionPane.showMessageDialog(this,
              "Jreepad v " + version + "\n\n" +
              "About" +
              "\n" +
              "\nJreepad \u00A9 2004-2007 Dan Stowell" +
              "\n" +
              "\nhttp://jreepad.sourceforge.net" +
              "\n\n" +
              "Murdered Out HACK by:" +
              "\n" +
              "Baiguai"
              ,
              "About Jreepad",
              JOptionPane.INFORMATION_MESSAGE);
  }

  private void showAutoSaveDialog()
  {
    // The autosave simply launches a background thread which periodically triggers saveAction if saveLocation != null
    autoSaveCheckBox.setSelected(getPrefs().autoSave);
    autoSavePeriodSpinner.getModel().setValue(new Integer(getPrefs().autoSavePeriod));
 //   autoSavePeriodSpinner.setValue(getPrefs().autoSavePeriod);
    autoSaveDialog.setVisible(true);
    autoSaveDialog.toFront();
  }

  private void showPrefsDialog()
  {
    prefsDialog.setVisible(true);
    prefsDialog.toFront();

    // Apply preferences that immediately affect the GUI
    funkyGreenStrip.setVisible(getPrefs().showGreenStrip);
    webSearchMenuItem.setText(getPrefs().webSearchName);
    characterWrapArticleMenuItem.setText("Hard Wrap"
        + getPrefs().characterWrapWidth + "Hard Wrap");

    theJreepad.currentArticleView.reloadArticle();
  }

  private void deleteNodeAction()
  {
    if(JOptionPane.showConfirmDialog(this, "Delete?"+":\n"+theJreepad.getCurrentNode().getTitle(), "Delete Confirmation",
               JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) return;
    theJreepad.removeNode();
    theJreepad.returnFocusToTree();
    updateWindowTitle();
  }

  private void getTreepadNodeUrl()
  {
//    String ret = theJreepad.getTreepadNodeUrl();
    nodeUrlDisplayField.setText(theJreepad.getTreepadNodeUrl().replace("\"", "").replace(" ", "_"));
    nodeUrlDisplayDialog.setVisible(true);
  }

  private boolean checkOverwrite(File theFile)
  {
    // If file doesn't already exist then fine
    if(theFile == null || !theFile.exists())
        return true;

    // Else we need to confirm
    return (JOptionPane.showConfirmDialog(this, "Overwrite?"+" "+theFile.getName()+" "+"Overwrite Confirmation",
                "Overwrite Confirmation",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);
  }

  public void showLicense()
  {
              JOptionPane.showMessageDialog(this,
"License" + "\n\n           http://www.gnu.org/copyleft/gpl.html\n" +
             "\n"
              ,
              "License",
              JOptionPane.INFORMATION_MESSAGE);
  }

  public void wrapContentToCharWidth()
  {
    theJreepad.wrapContentToCharWidth(getPrefs().characterWrapWidth);
  }
  public void stripAllTags()
  {
    if(JOptionPane.showConfirmDialog(this, "Strip Tags?",
                "Strip Tags Confirmation",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
      theJreepad.stripAllTags();
  }


  private void addToOpenRecentMenu(File f)
  {
    // Remove the file from the list if it's already in there...
    ListIterator iter = getPrefs().openRecentList.listIterator();
    File tempFile;
    while(iter.hasNext())
    {
      tempFile = (File)iter.next();
      if(tempFile == null || tempFile.equals(f))
        iter.remove();
    }

    Toolkit theToolkit = getToolkit();
    Dimension wndSize = theToolkit.getScreenSize();
    getPrefs().openRecentList.insertElementAt(f, 0);
    getPrefs().save();
    updateOpenRecentMenu();
  }
  private File[] openRecentTempFileList;
  private void updateOpenRecentMenu()
  {
    if(getPrefs().openRecentList.size() > getPrefs().openRecentListLength)
      getPrefs().openRecentList.setSize(getPrefs().openRecentListLength);

    try
    {
//      openRecentTempFileList = (File[])getPrefs().openRecentList.toArray();
      openRecentTempFileList = new File[getPrefs().openRecentList.size()];
      for(int i=0; i<getPrefs().openRecentList.size(); i++)
      {
        openRecentTempFileList[i] = (File)getPrefs().openRecentList.get(i);
      }
    }
    catch(ClassCastException e)
    {
      System.err.println(e);
      openRecentTempFileList = new File[0];
    }

    openRecentMenu.setEnabled(getPrefs().openRecentList.size()>1);

    openRecentMenu.removeAll();

    JMenuItem tempMenuItem;
    File tempFile;
    char theChar;
    for(int i=1; i<openRecentTempFileList.length; i++)
    {
      tempFile = openRecentTempFileList[i];
      tempMenuItem = new JMenuItem(tempFile.getParentFile().getName() + "/" + tempFile.getName());

      if(i<20)
      {
        theChar = ("" + i).charAt(0);
        tempMenuItem.setText("("+theChar+") "+tempMenuItem.getText());
        tempMenuItem.setMnemonic(theChar);
      }
      // ADD THE ACTIONLISTENER HERE
      tempMenuItem.addActionListener(new FileOpeningActionListener(tempFile));
      openRecentMenu.add(tempMenuItem);
    }
  }

  private void insertDate()
  {
    theJreepad.insertDate();
  }

  private class FileOpeningActionListener implements ActionListener
  {
    File f;
    FileOpeningActionListener(File f)          {this.f = f;}
    public void actionPerformed(ActionEvent e) {openFile(f);}
  } // End of:   private class FileOpeningActionListener extends ActionListener





// General info dialog.  The OSXAdapter calls this method when "About OSXAdapter"
  // is selected from the application menu.
  public void about() {
    aboutAction();
  }

  // General preferences dialog.  The OSXAdapter calls this method when "Preferences..."
  // is selected from the application menu.
  public void preferences() {
//    prefs.setSize(320, 240);
//    prefs.setLocation((int)this.getLocation().getX() + 22, (int)this.getLocation().getY() + 22);
//    prefs.setResizable(false);
//    prefs.setVisible(true);
  }

  // General info dialog.  The OSXAdapter calls this method when "Quit OSXAdapter"
  // is selected from the application menu, Cmd-Q is pressed, or "Quit" is selected from the Dock.
  public void quit() {
      quitAction();
  }


  protected void systemClipboardToNewNode()
  {
    Transferable cont = systemClipboard.getContents(this);

/*
    DataFlavor[] flavs = cont.getTransferDataFlavors();
    System.out.println("Data flavors supported by contents:\n");
    for(int i=0; i<flavs.length; i++)
      System.out.println("  " + flavs[i].getHumanPresentableName() + "\n"
                              + flavs[i].getMimeType() + "\n");
*/

    if(cont == null)
    {
      JOptionPane.showMessageDialog(this, "Clipboard is empty", "Clipboard Error" , JOptionPane.ERROR_MESSAGE);
      return;
    }
    try
    {
//      Reader readIt = DataFlavor.getTextPlainUnicodeFlavor().getReaderForText(cont);
      Reader readIt = DataFlavor.stringFlavor.getReaderForText(cont);
      char[] theStuff = new char[64];
      int numRead;
      StringBuffer theStuffBuf = new StringBuffer();
      while((numRead = readIt.read(theStuff)) != -1)
      {
        theStuffBuf.append(theStuff, 0, numRead);
      }

      String contStr = theStuffBuf.toString();
      String titStr;
      int newlinePos = contStr.indexOf("\n");
      if(newlinePos == -1)
      {
        titStr = contStr;
        contStr = "";
      }
      else
        titStr = contStr.substring(0, newlinePos-1);
      theJreepad.addChild(new JreepadNode(titStr, contStr));
    }
    catch(Exception err)
    {
      JOptionPane.showMessageDialog(this, "Clipboard Text Error", "Clipboard Error" , JOptionPane.ERROR_MESSAGE);
      return;
    }
  }

    public void jpHelp()
    {
        JOptionPane.showMessageDialog(this,
            "JREEPAD HELP:\n" +
            "jreepad is a plain text editor that utilizes a treeview to organize articles.\n" +
            "it is powered by java so it is cross platform, and its source code is freely available online.\n" +
            "\n" +
            "This version of jreepad is a rogue 'murdered out' version of jreepad, with some additional features\n" +
            "not found in the original jreepad such as:\n" +
            "\n" +
            "- add header divider\n" +
            "- hard wrap selected text\n" +
            "- find in article\n" +
            "- replace text in article\n" +
            "- upper/lower case functions\n" +
            "- minimalist console-like graphical interface\n" +
            "- jreepad file-based help system\n" +
            "- link follow fixes"
        ,
        "Jreepad Help",
        JOptionPane.INFORMATION_MESSAGE);

        /*
        try
        {
            String dir = System.getProperty("user.dir");
            updateOpenRecentMenu();
            File hlp = new File(dir + "/help.db");
            if (!hlp.exists()) return;
            openFile(hlp);
        }
        catch (Exception ex) {}
        */
    }

    public void keyHelp()
    {
        JOptionPane.showMessageDialog(this,
            "KEYS:\n" +
            "\n" +
            "--[ ADDING/DELETING NODES ]-----------------------------------------------------\n" +
            "\n" +
            "[ctrl+t]    add sibling node above the current node.\n" +
            "[ctrl+b]    add sibling node below the current node.\n" +
            "[ctrl+d]    add child node to the current node.\n" +
            "[ctrl+delete]    delete current node.\n" +
            "[f2]    renames the current node.\n" +
            "\n" +
            "\n" +
            "--[ MOVING NODES ]--------------------------------------------------------------\n" +
            "\n" +
            "[ctrl+alt+up arrow]    move node up.\n" +
            "[ctrl+alt+down arrow]    move node down.\n" +
            "[ctrl+alt+right arrow]    indent node.\n" +
            "[ctrl+alt+left arrow]    outdent node.\n" +
            "\n" +
            "\n" +
            "--[ NAVIGATING BETWEEN THE TREE AND ARTICLES ]----------------------------------\n" +
            "\n" +
            "[tab (while the tree is selected]    selects the article panel."
        ,
        "Keys Help",
        JOptionPane.INFORMATION_MESSAGE);

        /*
        try
        {
            String dir = System.getProperty("user.dir");
            updateOpenRecentMenu();
            File hlp = new File(dir + "/help.db");
            if (!hlp.exists()) return;
            openFile(hlp);
        }
        catch (Exception ex) {}
        */
    }

    public void linkHelp()
    {
        JOptionPane.showMessageDialog(this,
            "USING LINKS:\n" +
            "\n" +
            "if the link action option is set to node search, you\n" +
            "can create links to other articles in the tree by entering\n" +
            "the node's exact caption within the tree. then using the\n" +
            "ctrl+l hot key to follow the link.\n" +
            "\n" +
            "\n" +
            "--[ Note ]----------------------------------------------------------------------\n" +
            "If you would like to link to other articles be sure to select\n" +
            "the node:// links option in the drop down.\n" +
            "Navigate to the node you want to link to and hit Ctrl+Shift+U,\n" +
            "Select the node URL and copy it. Paste it into the article you\n" +
            "want to link to the other article. Later use Ctrl+l to go to\n" +
            "that article.\n" +
            "\n" +
            "\n" +
            "--[ Note ]----------------------------------------------------------------------\n" +
            "\n" +
            "Be sure there are no forward slashes (/) in the names of any of\n" +
            "of directories or article names, otherwise these will make the\n" +
            "link unreachable.\n" +
            "\n" +
            "\n" +
            "--[ Note ]----------------------------------------------------------------------\n" +
            "\n" +
            "For links with spaces in them, use an underscore (_) for\n" +
            "spaces.\n" +
            "\n" +
            "\n" +
            "--[ Note ]----------------------------------------------------------------------\n" +
            "\n" +
            "You can also link to files using:\n" +
            "file://<path to file>\n" +
            "(The file will be opened in the default web browser)\n"
        ,
        "Links Help",
        JOptionPane.INFORMATION_MESSAGE);

        /*
        try
        {
            String dir = System.getProperty("user.dir");
            updateOpenRecentMenu();
            File hlp = new File(dir + "/help.db");
            if (!hlp.exists()) return;
            openFile(hlp);
        }
        catch (Exception ex) {}
        */
    }

  public void keyboardHelp()
  {
        String tx = "";

        tx += "TREE NAVIGATION:\n";
        tx += "Use the arrow keys to navigate around the tree.\n";
        tx += "Up/down will move up/down the visible nodes.\n";
        tx += "Left/right will expand/collapse nodes.\n";
        tx += "\n";
        tx += "ADDING/DELETING NODES:\n";
        tx += "[Ctrl+T] Add sibling node above the current node.\n";
        tx += "[Ctrl+B] Add sibling node below the current node.\n";
        tx += "[Ctrl+D] Add child node to the current node.\n";
        tx += "[Ctrl+delete] Delete current node.\n";
        tx += "[F2] Renames the current node.\n";
        tx += "\n";
        tx += "MOVING NODES:\n";
        tx += "[Ctrl+Alt+up arrow] Move node up.\n";
        tx += "[Ctrl+Alt+down arrow] Move node down.\n";
        tx += "[Ctrl+Alt+right arrow] Indent node.\n";
        tx += "[Ctrl+Alt+left arrow] Outdent node.\n";
        tx += "\n";
        tx += "NAVIGATING BETWEEN THE TREE AND ARTICLES:\n";
        tx += "[Tab (while the tree is selected] Selects the article panel.\n";
        tx += "[Ctrl+Tab (while the article is selected] Selects the tree panel.\n";
        tx += "\n";
        tx += "USING LINKS:\n";
        tx += "If the Link Action option is set to Node Search, you\n";
        tx += "can create links to other articles in the tree by entering\n";
        tx += "the node's EXACT caption within the tree. Then using the\n";
        tx += "Ctrl+L hot key to follow the link.\n";
        tx += "\n\n\n\n";

        JOptionPane.showMessageDialog(this,
        tx
        ,
        "Keyboard",
        JOptionPane.INFORMATION_MESSAGE);
  }

  public void linksHelp()
  {
              JOptionPane.showMessageDialog(this,
              "Links" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              ""
              ,
              "Links",
              JOptionPane.INFORMATION_MESSAGE);
  }

  public void dragDropHelp()
  {
              JOptionPane.showMessageDialog(this,
              "Drag and Drop Help" +
              "\n" +
              "\nDRAG-AND-DROP:" +
              "\n" +
              "\nOne of the easiest ways to manage the structure" +
              "\nof your Treepad file is to drag the nodes around" +
              "\nusing the mouse." +
              "\n" +
              "\nClick on a node's title, and, keeping the mouse" +
              "\nbutton held down, move the mouse to where you" +
              "\nwant the node to be moved to. Then release the" +
              "\nmouse button, and the node will be moved to its" +
              "\nnew position in the tree." +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              ""
              ,
              "Drag and Drop",
              JOptionPane.INFORMATION_MESSAGE);
  }


/*
  // Replacement for the "JSpinner" which is not available in Java 1.3 or 1.2
  static class DSpinner extends Box
  {
    private int min, max, val;
    private JTextField textField;
    private JButton upBut, downBut;
    private ActionListener al;

    DSpinner(int min, int max, int myVal)
    {
      super(BoxLayout.X_AXIS);
      this.min=this.val=min;
      this.max=max;
      this.add(Box.createGlue());
      this.add(textField = new JTextField(val));
      this.add(downBut = new JButton("-"));
      this.add(upBut = new JButton("+"));
      this.add(Box.createGlue());

      downBut.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
                              setValue(val-1);
                              if(al!=null)
                                al.actionPerformed(null);
                              }});
      upBut.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
                              setValue(val+1);
                              if(al!=null)
                                al.actionPerformed(null);
                              }});
      setValue(myVal);
    }

    int getValue()
    {
      try
      {
        val = Integer.parseInt(textField.getText());
      }
      catch(NumberFormatException e)
      {
      }
      return val;
    }
    void setValue(int newVal)
    {
      if(newVal>=min && newVal<=max)
        val=newVal;
        textField.setText("" + val);
    }
    void addActionListener(ActionListener al)
    {
      this.al = al;
      textField.addActionListener(al);
    }
    void addCaretListener(CaretListener cl)
    {
      textField.addCaretListener(cl);
    }

  } // End of class DSpinner
*/

  static class ColouredStrip extends JPanel
  {
    Color col;
    ColouredStrip(Color col, int maxWidth, int maxHeight)
    {
      super(false);
      setMaximumSize(new Dimension(maxWidth, maxHeight));
      setBackground(col);
//      this.col = col;
    }

//    public void paint(Graphics gggg)
//    {
//      Graphics2D g = (Graphics2D)gggg;
//      g.setPaint(col);
//      g.fillRect(0,0,getWidth(),getHeight());
//    }
  }




/*
  // Methods required by Apple's "ApplicationListener" interface
  public void handleOpenFile(ApplicationEvent ae)
  {
     System.err.println("Jreepad.handleOpenFile() - ApplicationEvent is " + ae);
     openHjtFile(new java.io.File(ae.getFilename()));
     ae.setHandled(true);
  }
  public void handleOpenApplication(ApplicationEvent ae)
  {
     System.err.println("Jreepad.handleOpenApplication() - ApplicationEvent is " + ae);
  }
  public void handleReOpenApplication(ApplicationEvent ae)
  {
     System.err.println("Jreepad.handleReOpenApplication() - ApplicationEvent is " + ae);
  }
  public void handlePrintFile(ApplicationEvent ae)
  {
     System.err.println("Jreepad.handlePrintFile() - ApplicationEvent is " + ae);
     ae.setHandled(true);
     toBrowserForPrintAction();
  }
    public void handleAbout(ApplicationEvent ae) {
        ae.setHandled(true);
        about();
    }
    public void handlePreferences(ApplicationEvent ae) {
        preferences();
        ae.setHandled(true);
    }
    public void handleQuit(ApplicationEvent ae) {
        //
        //  You MUST setHandled(false) if you want to delay or cancel the quit.
        //  This is important for cross-platform development -- have a universal quit
        //  routine that chooses whether or not to quit, so the functionality is identical
        //  on all platforms.  This example simply cancels the AppleEvent-based quit and
        //  defers to that universal method.
        //
        ae.setHandled(false);
        quit();
    }
*/
}
