package jreepad;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

/**
 * The preferences dialog.
 *
 * @version $Id$
 */
public class PrefsDialog extends JDialog
{
    private JCheckBox loadLastFileOnOpenCheckBox;
    private JCheckBox autoDateNodesCheckBox;
    private JCheckBox autoDetectHtmlCheckBox;
    private JComboBox fileEncodingSelector;
    private JComboBox fileFormatSelector;
    private JCheckBox showGreenStripCheckBox;
    private JTextField dateFormatField;
    private JTextField tabsizeField;
    private JComboBox defaultSearchModeSelector;
    private JSpinner wrapWidthSpinner;
    private Box webSearchPrefsBox;
    private JTextField webSearchNameField;
    private JTextField webSearchPrefixField;
    private JTextField webSearchPostfixField;
    private JComboBox htmlExportModeSelector;
    private JCheckBox urlsToLinksCheckBox;
    private JComboBox htmlExportAnchorTypeSelector;
    private JButton prefsOkButton;
    private JButton prefsCancelButton;
    private JButton prefsDateInfoButton;
    private PrefsDialog self; // bleh - yucky but needed by actionlistener

    public PrefsDialog(Frame owner)
    {
        super(owner, "Preferences", true);
        setVisible(false);
        self = this;
        
        Box hBox, vBox;
        
        vBox = Box.createVerticalBox();
        vBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        Box genPrefVBox = Box.createVerticalBox();
        vBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        genPrefVBox.add(loadLastFileOnOpenCheckBox = new JCheckBox("Load last file open", getPrefs().loadLastFileOnOpen));
        loadLastFileOnOpenCheckBox.setHorizontalAlignment(SwingConstants.LEFT);
        loadLastFileOnOpenCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        genPrefVBox.add(autoDateNodesCheckBox = new JCheckBox("Auto date nodes", getPrefs().autoDateInArticles));
        autoDateNodesCheckBox.setHorizontalAlignment(SwingConstants.LEFT);
        autoDateNodesCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);

        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox.add(new JLabel("Tab Size", SwingConstants.LEFT));
        tabsizeField = new JTextField(getPrefs().tabsize, 4);
        hBox.add(tabsizeField);
        genPrefVBox.add(hBox);

        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox.add(new JLabel("Date Format", SwingConstants.LEFT));
        dateFormatField = new JTextField(getPrefs().dateFormat, 20);
        hBox.add(dateFormatField);

        hBox.add(prefsDateInfoButton = new JButton("Info"));
        prefsDateInfoButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    JOptionPane.showMessageDialog(self,
                    "Date Format"
                    ,
                    "Date",
                    JOptionPane.INFORMATION_MESSAGE);
                }
            });

        genPrefVBox.add(hBox);

        genPrefVBox.add(autoDetectHtmlCheckBox = new JCheckBox("Auto Detect HTML", getPrefs().autoDetectHtmlArticles));
        autoDetectHtmlCheckBox.setHorizontalAlignment(SwingConstants.LEFT);
        autoDetectHtmlCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);

        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        // hBox.add(Box.createGlue());
        hBox.add(new JLabel("Encoding", SwingConstants.LEFT));
        hBox.add(fileEncodingSelector = new JComboBox(JreepadPrefs.characterEncodings));
        fileEncodingSelector.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox.add(Box.createGlue());
        genPrefVBox.add(hBox);
        fileEncodingSelector.setSelectedIndex(getPrefs().fileEncoding);

        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        // hBox.add(Box.createGlue());
        hBox.add(new JLabel("File Type", SwingConstants.LEFT));
        hBox.add(fileFormatSelector = new JComboBox(JreepadPrefs.mainFileTypes));
        fileFormatSelector.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox.add(Box.createGlue());
        genPrefVBox.add(hBox);
        fileFormatSelector.setSelectedIndex(getPrefs().mainFileType);

        genPrefVBox.add(showGreenStripCheckBox = new JCheckBox("Dumb Green Thing",
            getPrefs().showGreenStrip));
        showGreenStripCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        showGreenStripCheckBox.setHorizontalAlignment(SwingConstants.LEFT);

        JPanel genPanel = new JPanel();
        genPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        genPanel.add(genPrefVBox);
        genPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            "General Preferences"));
        vBox.add(genPanel);

        genPanel = new JPanel();
        genPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox.add(new JLabel("Link Action", SwingConstants.LEFT));
        hBox.add(defaultSearchModeSelector = new JComboBox(new String[] {
                        "Web Search",
                        "Node Search" }));
        hBox.add(Box.createGlue());
        defaultSearchModeSelector.setSelectedIndex(getPrefs().defaultSearchMode);
        defaultSearchModeSelector.setAlignmentX(Component.LEFT_ALIGNMENT);
        genPanel.add(hBox);
        genPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            "Link Action"));
        vBox.add(genPanel);

        genPanel = new JPanel();
        genPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        genPrefVBox = Box.createVerticalBox();
        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox.add(new JLabel("Hard Wrap Width", SwingConstants.LEFT));
        hBox.add(wrapWidthSpinner = new JSpinner(new SpinnerNumberModel(
            getPrefs().characterWrapWidth, 1, 1000, 1)));
        hBox.add(Box.createGlue());
        wrapWidthSpinner.setAlignmentX(Component.LEFT_ALIGNMENT);
        wrapWidthSpinner.getEditor().setAlignmentX(Component.LEFT_ALIGNMENT);
        // hBox.add(wrapWidthSpinner = new DSpinner(1,1000,getPrefs().characterWrapWidth));
        hBox.add(Box.createGlue());
        genPrefVBox.add(hBox);
        genPanel.add(genPrefVBox);
        genPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            "Hard Wrap"));
        vBox.add(genPanel);

        webSearchPrefsBox = Box.createVerticalBox();
        webSearchPrefsBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox.add(new JLabel("Web Search\n", SwingConstants.LEFT));
        hBox.add(webSearchNameField = new JTextField(getPrefs().webSearchName));
        webSearchPrefsBox.add(hBox);

        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox.add(new JLabel("Web Search\n", SwingConstants.LEFT));
        hBox.add(Box.createGlue());
        webSearchPrefsBox.add(hBox);

        hBox = Box.createHorizontalBox();
        hBox.add(new JLabel("http://", SwingConstants.LEFT));
        hBox.add(webSearchPrefixField = new JTextField(getPrefs().webSearchPrefix));
        webSearchPrefixField.setAlignmentX(Component.LEFT_ALIGNMENT);

        hBox.add(new JLabel("Selected Text", SwingConstants.LEFT));
        hBox.add(webSearchPostfixField = new JTextField(getPrefs().webSearchPostfix));
        webSearchPostfixField.setAlignmentX(Component.LEFT_ALIGNMENT);
        webSearchPrefsBox.add(hBox);
        JPanel webSearchPanel = new JPanel();
        webSearchPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        webSearchPanel.add(webSearchPrefsBox);
        webSearchPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
            .createEtchedBorder(), "Web Search"));
        vBox.add(webSearchPanel);

        // Now the HTML export options
        genPanel = new JPanel();
        genPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        genPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            "HTML Export"));
        Box htmlVBox = Box.createVerticalBox();
        htmlVBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        // hBox.add(Box.createGlue());
        hBox.add(new JLabel("Treat as Text", SwingConstants.LEFT));
        htmlExportModeSelector = new JComboBox(JreepadArticle.getHtmlExportArticleTypes());
        htmlExportModeSelector.setAlignmentX(Component.LEFT_ALIGNMENT);
        htmlExportModeSelector.setSelectedIndex(getPrefs().htmlExportArticleType);
        htmlExportModeSelector.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    if (htmlExportModeSelector.getSelectedIndex() == 2)
                    {
                        urlsToLinksCheckBox.setEnabled(false);
                        urlsToLinksCheckBox.setSelected(false);
                    }
                    else
                    {
                        urlsToLinksCheckBox.setEnabled(true);
                        urlsToLinksCheckBox.setSelected(getPrefs().htmlExportUrlsToLinks);
                    }
                }
            });
        hBox.add(htmlExportModeSelector);
        hBox.add(Box.createGlue());
        htmlVBox.add(hBox);
        htmlVBox.add(urlsToLinksCheckBox = new JCheckBox("Auto Link",
            getPrefs().htmlExportUrlsToLinks));
        urlsToLinksCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        urlsToLinksCheckBox.setHorizontalAlignment(SwingConstants.LEFT);
        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        // hBox.add(Box.createGlue());
        hBox.add(new JLabel("Internal Links", SwingConstants.LEFT));
        htmlExportAnchorTypeSelector = new JComboBox(JreepadArticle.getHtmlExportAnchorLinkTypes());
        htmlExportAnchorTypeSelector.setSelectedIndex(getPrefs().htmlExportAnchorLinkType);
        htmlExportAnchorTypeSelector.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox.add(htmlExportAnchorTypeSelector);
        hBox.add(Box.createGlue());
        htmlVBox.add(hBox);
        genPanel.add(htmlVBox);
        vBox.add(genPanel);

        genPanel = new JPanel();
        genPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        hBox = Box.createHorizontalBox();
        hBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        hBox.add(prefsOkButton = new JButton("Ok"));
        hBox.add(prefsCancelButton = new JButton("Cancel"));
        prefsOkButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    getPrefs().loadLastFileOnOpen = loadLastFileOnOpenCheckBox.isSelected();
                    getPrefs().autoDateInArticles = autoDateNodesCheckBox.isSelected();
                    getPrefs().autoDetectHtmlArticles = autoDetectHtmlCheckBox.isSelected();
                    getPrefs().webSearchName = webSearchNameField.getText();
                    getPrefs().webSearchPrefix = webSearchPrefixField.getText();
                    getPrefs().webSearchPostfix = webSearchPostfixField.getText();
                    getPrefs().defaultSearchMode = defaultSearchModeSelector.getSelectedIndex();
                    getPrefs().fileEncoding = fileEncodingSelector.getSelectedIndex();
                    getPrefs().mainFileType = fileFormatSelector.getSelectedIndex();
                    getPrefs().characterWrapWidth = ((Integer)(wrapWidthSpinner.getValue()))
                        .intValue();
                    // getPrefs().characterWrapWidth = wrapWidthSpinner.getValue();
                    // setFontsFromPrefsBox();
                    // getPrefs().wrapToWindow = wrapToWindowCheckBox.isSelected();
                    // theJreepad.setEditorPaneKit();
                    getPrefs().htmlExportArticleType = htmlExportModeSelector.getSelectedIndex();
                    getPrefs().htmlExportAnchorLinkType = htmlExportAnchorTypeSelector
                        .getSelectedIndex();
                    // getPrefs().addQuotesToCsvOutput = quoteCsvCheckBox.isSelected();
                    getPrefs().showGreenStrip = showGreenStripCheckBox.isSelected();

                    String tabSize = tabsizeField.getText();
                    getPrefs().tabsize = tabSize;

                    String dateFormat = dateFormatField.getText();
                    try
                    {
                        new SimpleDateFormat(dateFormat);
                    }
                    catch (IllegalArgumentException ex)
                    {
                        JOptionPane.showMessageDialog(PrefsDialog.this,
                            "Invalid Date Format",
                            "Date Error" ,
                            JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    getPrefs().dateFormat = dateFormat;

                    // If exporting as HTML then we ignore this checkbox
                    if (htmlExportModeSelector.getSelectedIndex() != 2)
                        getPrefs().htmlExportUrlsToLinks = urlsToLinksCheckBox.isSelected();
                    getPrefs().save();
                    setVisible(false);
                }
            });
        prefsCancelButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    setVisible(false);
                }
            });
        genPanel.add(hBox);
        vBox.add(genPanel);
        getContentPane().add(vBox);
        // Finished establishing the prefs dialogue box
    }

    private static JreepadPrefs getPrefs()
    {
        return JreepadView.getPrefs();
    }
}
