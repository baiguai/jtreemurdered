javac -cp .:release/lib/* -d ./compiled ./code/*.java
cd compiled
jar -cfm ../release/jreepad.jar manifest.txt jreepad/*.class jreepad/editor/*.class jreepad/io/*.class jreepad/ui/*.class
