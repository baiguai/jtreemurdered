The entry point is: JreepadViewer

When adding new features, edit the following files:
    ~/jreepad/code/ArticleView.java
        Add a reference to the new method.

    ~/jreepad/code/EditorPaneView.java
        Add the actual method - perform the action.

    ~/jreepad/code/JreepadView.java
        Create a method of the same name that tells the currentArticleView to call the method.

    ~/jreepad/code/JreepadViewer.java
        Setup the menu and call the new method.

    ~/jreepad/code/TableViewer.java
        Create an empty method of the new method's name - template reference.
